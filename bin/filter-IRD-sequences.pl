#!/usr/bin/perl

# 07.09.2010 20:00:48 EDT
# Harm van Bakel <hvbakel@gmail.com>

# MODULES
use strict;
use warnings;
use Getopt::Long;

# GET PARAMETERS
my $sHelp         = 0;
my $nMinLength    = 790;
my $nMaxLength    = 2800;
GetOptions("help!"       => \$sHelp,
           "minlength:i" => \$nMinLength,
           "maxlength:i" => \$nMaxLength);

# PRINT HELP
$sHelp = 1 unless(@ARGV);
if ($sHelp) {
   my $sScriptName = ($0 =~ /^.*\/(.+$)/) ? $1 : $0;
   die <<HELP

   Usage: $sScriptName <fasta-file>
   
   Filter IRD sequences to remove low-quality segments based on
   sequence length, the presence of 'N' nucleotides and terminal
   conserved sequences.
   
   Arugments:
    -minlength <integer>
      Minumum length of fasta sequence
      Default: $nMinLength
    -maxlength <integer>
      Maximum length of fasta sequence
      Default: $nMaxLength
    -help
      This help message
      
HELP
}


##########
## MAIN ##
##########

my ($nTotal, $nTooShort, $nTooLong, $nHasN, $nNo5cons, $nNo3cons, $nRemoved, $nKept) = (0, 0, 0, 0, 0, 0, 0, 0);
foreach my $sInput (@ARGV){
   my $sFastaHeader = '';
   my $sFastaSeq    = '';
   my $flRevSeq     = 0;
   open INPUT, "<$sInput" or die "Error: can't read the fasta file\n";
   while (<INPUT>){
      if (/^>/ or eof){
         if (eof){
            die "Error: file ends in fasta header without sequence\n" if (/^>/);
            $sFastaSeq .= $_;
         }
         if ($sFastaHeader){
            my $flPrint = 1;
            my $sFastaCheck = $sFastaSeq;
            $sFastaCheck    =~ s/\s//g;
            $sFastaCheck    =~ s/[\n\r]+//g;
            my $nLength = length($sFastaCheck);
            my $flHasN  = ($sFastaCheck =~ /N/) ? 1 : 0;
            my $fl5cons = ($sFastaCheck =~ /^AGCA/) ? 1 : 0;
            my $fl3cons = ($sFastaCheck =~ /TACT$/) ? 1 : 0;
            
            # Gather stats
            $nTotal++;
            $nTooShort++ if ($nLength < $nMinLength);
            $nTooLong++  if ($nLength > $nMaxLength);
            $nHasN++     if ($flHasN);
            $nNo5cons++  if ($fl5cons);
            $nNo3cons++  if ($fl3cons);
            
            # Do filtering
            if ( ($nLength > $nMinLength) and ($nLength < $nMaxLength) and ($flHasN == 0) ){
               print $sFastaHeader;
               print $sFastaSeq;
               $nKept++;
            }
            else{
               $nRemoved++;
            }
         }
         $sFastaHeader = $_;
         $sFastaSeq    = "";
      }
      else{
         next if (/^\s*$/);
         next if (/^ *#/);
         $sFastaSeq .= $_ if ($sFastaHeader);
      }
   }
   close INPUT;
}

# Print stats
print STDERR "Segment is too Short:     $nTooShort\n";
print STDERR "Segment is too Long:      $nTooLong\n";
print STDERR "Contains N nucleotides:   $nHasN\n";
print STDERR "No 5' conserved sequence: $nNo5cons\n";
print STDERR "No 3' conserved sequence: $nNo3cons\n";
print STDERR "Removed: $nRemoved\n";
print STDERR "Kept:    $nKept\n";
