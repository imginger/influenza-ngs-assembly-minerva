#!/usr/bin/env python

"""
Created on Aug  10 2016

@author: Divya Kriti

usage: ./reads_report.py prefix

prefix: the basename of the sequencing files. e.g. H112_03_curated.fastq.gz => prefix = H112_03_curated

"""

import sys
import pandas as pd
import matplotlib.pyplot as plt


def usage():
   return "usage:"+"\n"+"\treads_report.py <plotdip file> <star_Log.final.out file> <cutadapt.log file> <output file>"

if len(sys.argv)!=5:
   print usage()
   sys.exit(0)


def parseReadInfo(plotdipfile, starlogfile, cutadaptlogfile, outfile):
   table_plotdip  = pd.read_csv(plotdipfile, sep='\t', header = 0)
   table_starLog  = pd.read_csv(starlogfile, sep='|', header = None)
   table_cutadapt = open(cutadaptlogfile)

   # Get chimeric reads from the dip file
   chimeric_reads = float(table_plotdip['Count'].sum(axis=0))

   # Get mapping stats from the star file
   table_starLog.columns = ['Col1', 'Col2']
   table_starLog['Col1'] = table_starLog['Col1'].map(lambda x: x.lstrip('\t').rstrip('\t'))
   table_starLog.fillna(0)
   total_passedQc_reads  = float(table_starLog.iloc[4,1].lstrip('\t'))
   uniquely_mapped_reads = float(table_starLog.iloc[7,1].lstrip('\t'))
   multimapping_reads    = float(table_starLog.iloc[22,1].lstrip('\t')) + float(table_starLog.iloc[24,1].lstrip('\t'))

   # Get stats from cutadapt log
   for line in table_cutadapt:
      if line.startswith("Total read pairs processed:"):
         total_reads_line = line.strip()
      elif line.startswith("Pairs that were too short:"):
         failedQc_reads_line = line.strip()
      elif line.startswith("Total reads processed:"):
         total_reads_line = line.strip()
      elif line.startswith("Reads that were too short:"):
         failedQc_reads_line = line.strip()

   # Calculate remaining read stats
   failedQc_reads_list         = failedQc_reads_line.split(':')
   failedQc_reads_unedited     = failedQc_reads_list[1].strip().split('(')
   failedQc_reads              = float(failedQc_reads_unedited[0].replace(',', '').strip())
   total_reads_list            = total_reads_line.split(':')
   total_reads                 = float(total_reads_list[1].replace(',', '').lstrip())
   nonChimeric_reads           = total_passedQc_reads - chimeric_reads
   passedQc_unmapped_reads_tot = total_passedQc_reads - uniquely_mapped_reads - multimapping_reads - chimeric_reads

   # Write output file
   file = open(outfile, "w")
   file.write("col1\tcol2")
   file.write("\n")
   file.write("chimeric_reads\t%s" %chimeric_reads)
   file.write("\n")
   file.write("total_passedQc_reads\t%s" %total_passedQc_reads)
   file.write("\n")
   file.write("uniquely_mapped_reads\t%s" %uniquely_mapped_reads)
   file.write("\n")
   file.write("passedQc_unmapped_reads\t%s" %passedQc_unmapped_reads_tot)
   file.write("\n")
   file.write("multimapping_reads\t%s" %multimapping_reads)
   file.write("\n")
   file.write("failedQc_reads\t%s" %failedQc_reads)
   file.write("\n")
   file.write("total_reads-parsed\t%s" %total_reads)
   file.write("\n")
   file.write("nonChimeric_reads\t%s" %nonChimeric_reads)
   file.close()

   return 0


if __name__ == "__main__":

   plotdipfile = sys.argv[1]
   starlogfile = sys.argv[2]
   cutadaptlogfile = sys.argv[3]
   outfile = sys.argv[4]

   try:
      parseReadInfo(plotdipfile, starlogfile, cutadaptlogfile, outfile)
   except:
      print "Error reading files\n"
      print usage()
      sys.exit(0)

