#!/usr/bin/make -Rf

# Harm van Bakel <harm.vanbakel@mssm.edu>
# Divya Kriti <divya.kriti@mssm.edu>

###############
# DEFINE ARGS #
###############

type        ?=A
np          ?=12
kmersize    ?=27
kmercov     ?=5
varthr      ?=0.05
awkt        :=awk -F '\t' -v OFS='\t'
sortt       :=sort -t $$'\t'

##############
# CHECK ARGS #
##############

ifeq ($(type),A)
	inflblatidx =/sc/orga/projects/vanbah01a/reference-databases/influenza/IAV-2017-08-21/IAV-2017-08-21_filt_cdhit_assembly-check.2bit
	inflstaridx =/sc/orga/projects/vanbah01a/reference-databases/influenza/IAV-2017-08-21/IAV-2017-08-21_filt_cdhit_chimera-check_STAR
else
	inflblatidx =/sc/orga/projects/vanbah01a/reference-databases/influenza/IBV-2017-08-23/IBV-2017-08-23_filt_cdhit_assembly-check.2bit
	inflstaridx =/sc/orga/projects/vanbah01a/reference-databases/influenza/IBV-2017-08-23/IBV-2017-08-23_filt_cdhit_chimera-check_STAR
endif

ifndef in
$(error missing argument in)
else
name?=$(in:.fastq.gz=)
endif

ifndef PICARD_HOME
$(error PICARD_HOME environment variable not set, please run the pipeline through the provided 'run-flu-assembly' wrapper script)
endif
ifndef GATK_JAR
$(error GATK_JAR environment variable not set, please run the pipeline through the provided 'run-flu-assembly' wrapper script)
endif

###########
# FOLDERS #
###########

#### Subfolders definition. When running the final post-curation assembly tasks the fAssemblyBam and fAssembly folders are remapped
ifneq (,$(findstring final,$(MAKECMDGOALS)))
   fFilteredReads    ?=01_filtered_reads
   fVarPatch         ?=04_final_varpatch
   fAssemblyBam      ?=05_final_bam
   fAssembly         ?=06_final
else
   fFilteredReads    ?=01_filtered_reads
   fAssemblyBam      ?=02_assembly_bam
   fAssembly         ?=03_assembly
endif

#########
# RULES #
#########

assemble: cutadapt chimeradetect inchworm assembly_bam assembly_report assembly_complete

final: final_varpatch final_bam final_report

cutadapt:          $(fFilteredReads)/ $(fFilteredReads)/$(name).cutadapt.fastq.gz \
	                $(fFilteredReads)/$(name)_cutadapt_1_nochreads.fastq.gz \
	                $(fFilteredReads)/$(name)_cutadapt.log
	$(info ${\n}#############  FINISHED CUTADAPT TASK #############${\n} )

chimeradetect:     $(fFilteredReads)/ $(fFilteredReads)/$(name)_chimeradetect.fa $(fFilteredReads)/$(name)_chimeradetect.chimerajunct $(fFilteredReads)/$(name)_cutadapt_2_nochimera.fastq.gz
	$(info ${\n}#############  FINISHED CHIMERADETECT TASK #############${\n} )

inchworm:          $(fAssembly)/ $(fAssembly)/$(name)_assembly.fa
	$(info ${\n}#############  FINISHED INCHWORM TASK #############${\n} )

assembly_bam:      $(fAssemblyBam)/ $(fAssemblyBam)/$(name)_assembly_bwa.bam $(fAssemblyBam)/$(name)_assembly_bwa.bam.bai \
			          $(fAssemblyBam)/$(name)_assembly_star.bam $(fAssemblyBam)/$(name)_assembly_star.bam.bai \
			          $(fAssemblyBam)/$(name)_assembly_star_Log.final.out
	$(info ${\n}#############  FINISHED ASSEMBLY_BAM TASK #############${\n} )

assembly_report:   $(fAssembly)/ $(fAssembly)/$(name)_assembly.info.cov $(fAssembly)/$(name)_assembly.stats-variants.txt \
			          $(fAssembly)/$(name)_assembly.stats-DIP.txt $(fAssembly)/$(name)_assembly.stats-coverage.txt $(fAssembly)/$(name)_assembly.stats-mapping.txt \
			          $(fAssembly)/$(name)_assembly.report.pdf \
			          $(fAssembly)/$(name)_assembly.features_table.txt
	$(info ${\n}#############  FINISHED ASSEMBLY_REPORT TASK #############${\n} )

assembly_complete: $(fAssembly)/ $(fAssembly)/$(name)_assembly_complete.fa $(fAssembly)/$(name)_assembly_complete.info \
                   $(fAssembly)/$(name)_assembly_complete.check \
                   $(fAssembly)/$(name)_assembly_complete.features_table.txt
	$(info ${\n}#############  FINISHED ASSEMBLY_COMPLETE TASK #############${\n} )

final_varpatch:    $(fVarPatch) $(fAssembly) $(fAssemblyBam) \
						 03_assembly/$(name)_assembly_curated.fa \
						 $(fVarPatch)/$(name)_assembly_curated_bwa.bam $(fVarPatch)/$(name)_assembly_curated_bwa.bam.bai \
                   $(fVarPatch)/$(name)_assembly_curated.stats-variants.txt $(fAssembly)/$(name)_final.fa
	$(info ${\n}#############  FINISHED FINAL_VARPATCH TASK #############${\n} )

final_bam:		    $(fAssemblyBam)/ $(fAssemblyBam)/$(name)_final_bwa.bam $(fAssemblyBam)/$(name)_final_bwa.bam.bai \
		             $(fAssemblyBam)/$(name)_final_star.bam $(fAssemblyBam)/$(name)_final_star.bam.bai \
		             $(fAssemblyBam)/$(name)_final_star_Log.final.out
	$(info ${\n}#############  FINISHED FINAL_BAM TASK #############${\n} )

final_report:      $(fAssembly)/ $(fAssembly)/$(name)_final.info.cov $(fAssembly)/$(name)_final.stats-variants.txt \
		             $(fAssembly)/$(name)_final.stats-DIP.txt $(fAssembly)/$(name)_final.stats-coverage.txt $(fAssembly)/$(name)_final.stats-mapping.txt \
		             $(fAssembly)/$(name)_final.report.pdf \
		             $(fAssembly)/$(name)_final.variants.calls.txt \
		             $(fAssembly)/$(name)_final.features_table.txt \
		             $(fAssembly)/$(name)_final_subtype.txt
	$(info ${\n}#############  FINISHED FINAL_REPORT TASK #############${\n} )

annotate:
	influenza_run-flu-annotation-tool.pl -i $(fAssembly)/$(name)_final.fa

upload: annotate
	cripdb-submit.php $(name)

lofreq: $(name)_curated_varpatch_bwa.bam $(name)_curated_varpatch_bwa.bam.bai $(name)_curated_varpatch_bwa_loseq_knownsites_filt.vcf \
        $(name)_curated_varpatch_bwa_rg_recal.table $(name)_curated_varpatch_bwa_rg_bqsr.bam $(name)_curated_varpatch_bwa_rg_bqsr.bam.bai \
        $(name)_curated_varpatch_bwa_loseq_snvindel.vcf

.PHONY: clean compact

compact:
	rm -rf 01_filtered_reads/*_cutadapt_?_* 02_assembly_bam/*.bam* 05_final_bam/*.bam*

clean: compact
	rm -rf 01_filtered_reads 02_assembly_bam 03_assembly 04_final_varpatch 05_final_bam 06_final


#####################
# CREATE SUBFOLDERS #
#####################

# create the directories, if they don't exist
$(fFilteredReads) $(fAssembly) $(fAssemblyBam) $(fVarPatch):
	@mkdir -p $@

############################
# REMOVE ADAPTER SEQUENCES #
############################
$(fFilteredReads)/%.cutadapt.fastq.gz: %.fastq.gz
	cutadapt -f fastq --match-read-wildcards -e 0.1 -O 6 -m 32 -l 250 \
	   -g ^GAGCTAGTCTG -g ^GTCGAGCTCG -g ^GTTACGCGCC -g ^GGGGGG -g ^CGGGTTATT -g ^GGTAACGCGTGATC -a GATCGGAAGAGCACACGTCT -b ACACTCTTTCCCTACACGACGCTCTTCCGATCT -b GCCAGAGCCGTAAGGACGACTTGGCGAGAAGGCTAGA \
	   $< 2> $(patsubst %.fastq.gz,$(fFilteredReads)/%_cutadapt.log,$<) | gzip > $@

$(fFilteredReads)/%_cutadapt.log: $(fFilteredReads)/%.cutadapt.fastq.gz
	echo "Following dummy prerequisite for $@"

$(fFilteredReads)/%_cutadapt.chreadids: $(fFilteredReads)/%.cutadapt.fastq.gz $(inflstaridx)
	rm -rf $(fFilteredReads)/$(name)_chreaddetect/
	mkdir $(fFilteredReads)/$(name)_chreaddetect
	STAR \
        --chimSegmentMin        8 \
        --chimJunctionOverhangMin 8 \
        --alignSJoverhangMin 8 \
        --outSJfilterReads Unique \
        --outSJfilterOverhangMin 8 8 8 8 \
        --outSJfilterCountUniqueMin 2 2 2 2 \
        --outSJfilterCountTotalMin 2 2 2 2 \
        --outSJfilterDistToOtherSJmin 0 0 0 0 \
        --genomeDir 	$(inflstaridx) \
        --runThreadN $(np) \
        --outReadsUnmapped unmapped \
        --outStd SAM \
        --outSAMmode Full \
        --outFileNamePrefix $(fFilteredReads)/$(name)_chreaddetect/refmap_star \
        --readFilesCommand zcat \
        --readFilesIn $< | samtools view -bS - \
        > $(fFilteredReads)/$(name)_chreaddetect/refmap_star.bam
	samtools view $(fFilteredReads)/$(name)_chreaddetect/refmap_star.bam | $(awkt) '$$6~/[0-9][0-9][0-9]+N/{print $$1}' | sort | uniq > $@
	$(awkt) '$$1!~/^@/{print $$1}' $(fFilteredReads)/$(name)_chreaddetect/refmap_starChimeric.out.sam | uniq >> $@
	rm -rf $(fFilteredReads)/$(name)_chreaddetect

$(fFilteredReads)/%_cutadapt_1_nochreads.fastq.gz: $(fFilteredReads)/%.cutadapt.fastq.gz $(fFilteredReads)/%_cutadapt.chreadids
	fastq-filter-by-id.pl -f $(word 1, $+) -m $(word 2, $+) -v | pigz -b 2048 -p $(np) > $@

###########################################
# INCHWORM ASSEMBLY FOR CHIMERA DETECTION #
###########################################

$(fFilteredReads)/%_chimeradetect: $(fFilteredReads)/%_cutadapt_1_nochreads.fastq.gz
	Trinity --no_run_chrysalis --no_run_butterfly --CPU $(np) --KMER_SIZE $(kmersize) --min_kmer_cov $(kmercov) --no_cleanup --seqType fq --JM 5G --output $@ --single $<

$(fFilteredReads)/%_chimeradetect.assembly.fa: $(fFilteredReads)/%_chimeradetect
	filter-inchworm-contigs.pl -minlength 100 -mincount 250 $</inchworm.K$(kmersize).L25.DS.fa > $@

$(fFilteredReads)/%_chimeradetect.fa $(fFilteredReads)/%_chimeradetect.chimerajunct: $(fFilteredReads)/%_chimeradetect.assembly.fa $(inflblatidx)
	influenza_annotate-contigs.pl -f $(word 1, $+) -r $(word 2, $+) -t $(type) -o $(patsubst %.assembly.fa,%,$<)

$(fFilteredReads)/%_cutadapt_2_nochimera.fastq.gz: $(fFilteredReads)/%_cutadapt_1_nochreads.fastq.gz $(fFilteredReads)/%_chimeradetect.chimerajunct
	fastq-filter-by-seq.pl -f $(word 1, $+) -m $(word 2, $+) -r -e 1 -v | pigz -b 2048 -p $(np) > $@

###################################################
# INCHWORM ASSEMBLY AFTER REMOVING CHIMERIC READS #
###################################################

$(fAssembly)/%_assembly.fa: $(fFilteredReads)/%_cutadapt_2_nochimera.fastq.gz $(inflblatidx)
	mkdir $(fAssembly)/data/
	Trinity --no_run_chrysalis --no_run_butterfly --CPU $(np) --KMER_SIZE 25 --min_kmer_cov 6 --no_cleanup --seqType fq --JM 5G --output $(fAssembly)/data/k25_c06 --single $<
	Trinity --no_run_chrysalis --no_run_butterfly --CPU $(np) --KMER_SIZE 27 --min_kmer_cov 6 --no_cleanup --seqType fq --JM 5G --output $(fAssembly)/data/k27_c06 --single $<
	Trinity --no_run_chrysalis --no_run_butterfly --CPU $(np) --KMER_SIZE 29 --min_kmer_cov 6 --no_cleanup --seqType fq --JM 5G --output $(fAssembly)/data/k29_c06 --single $<
	Trinity --no_run_chrysalis --no_run_butterfly --CPU $(np) --KMER_SIZE 31 --min_kmer_cov 6 --no_cleanup --seqType fq --JM 5G --output $(fAssembly)/data/k31_c06 --single $<
	Trinity --no_run_chrysalis --no_run_butterfly --CPU $(np) --KMER_SIZE 25 --min_kmer_cov 12 --no_cleanup --seqType fq --JM 5G --output $(fAssembly)/data/k25_c12 --single $<
	Trinity --no_run_chrysalis --no_run_butterfly --CPU $(np) --KMER_SIZE 27 --min_kmer_cov 12 --no_cleanup --seqType fq --JM 5G --output $(fAssembly)/data/k27_c12 --single $<
	Trinity --no_run_chrysalis --no_run_butterfly --CPU $(np) --KMER_SIZE 29 --min_kmer_cov 12 --no_cleanup --seqType fq --JM 5G --output $(fAssembly)/data/k29_c12 --single $<
	Trinity --no_run_chrysalis --no_run_butterfly --CPU $(np) --KMER_SIZE 31 --min_kmer_cov 12 --no_cleanup --seqType fq --JM 5G --output $(fAssembly)/data/k31_c12 --single $<
	cat $(fAssembly)/data/k25_c06/inchworm.K25.L25.DS.fa | perl -pe 's/^>a/>k25_c06_a/' >  $(fAssembly)/data/assembly_merged.fa
	cat $(fAssembly)/data/k27_c06/inchworm.K27.L25.DS.fa | perl -pe 's/^>a/>k27_c06_a/' >> $(fAssembly)/data/assembly_merged.fa
	cat $(fAssembly)/data/k29_c06/inchworm.K29.L25.DS.fa | perl -pe 's/^>a/>k29_c06_a/' >> $(fAssembly)/data/assembly_merged.fa
	cat $(fAssembly)/data/k31_c06/inchworm.K31.L25.DS.fa | perl -pe 's/^>a/>k31_c06_a/' >> $(fAssembly)/data/assembly_merged.fa
	cat $(fAssembly)/data/k25_c12/inchworm.K25.L25.DS.fa | perl -pe 's/^>a/>k25_c12_a/' >> $(fAssembly)/data/assembly_merged.fa
	cat $(fAssembly)/data/k27_c12/inchworm.K27.L25.DS.fa | perl -pe 's/^>a/>k27_c12_a/' >> $(fAssembly)/data/assembly_merged.fa
	cat $(fAssembly)/data/k29_c12/inchworm.K29.L25.DS.fa | perl -pe 's/^>a/>k29_c12_a/' >> $(fAssembly)/data/assembly_merged.fa
	cat $(fAssembly)/data/k31_c12/inchworm.K31.L25.DS.fa | perl -pe 's/^>a/>k31_c12_a/' >> $(fAssembly)/data/assembly_merged.fa
	filter-inchworm-contigs.pl -minlength 100 -mincount 250 $(fAssembly)/data/assembly_merged.fa > $(fAssembly)/data/assembly_filt.fa
	influenza_annotate-contigs.pl -prefilt -f $(fAssembly)/data/assembly_filt.fa -r $(word 2, $+) -t $(type) -o $(fAssembly)/data/assembly_filt_annot
	cd-hit-est -c 0.98 -i $(fAssembly)/data/assembly_filt_annot.fa -o $(fAssembly)/data/assembly_filt_annot_cdhit.fa
	cp $(fAssembly)/data/assembly_filt_annot_cdhit.fa $@


##############################
# MAP READS BACK TO ASSEMBLY #
##############################

# BAM indexing
%.bam.bai: %.bam
	samtools index $<

# BWA mapping (for read coverage and variant analysis)
$(fAssemblyBam)/%_bwa_index.amb: $(fAssembly)/%.fa
	bwa index -p $(patsubst %.amb,%,$@) $< 

$(fAssemblyBam)/%_bwa_tmp.bam: $(fAssemblyBam)/%_bwa_index.amb $(fFilteredReads)/$(name).cutadapt.fastq.gz
	bwa mem -t $(np) -M $(patsubst %.amb,%,$<) $(word 2, $+) | samtools view -S -bq 1 - > $@
	rm -f $(patsubst %.amb,%*,$<)

$(fAssemblyBam)/%_bwa.bam: $(fAssemblyBam)/%_bwa_tmp.bam
	samtools sort $< $(patsubst %.bam,%,$@)

# STAR mapping (for DIP analysis)
$(fAssemblyBam)/%_star.bam: $(fFilteredReads)/$(name).cutadapt.fastq.gz $(fAssembly)/%.fa
	rm -rf $(fAssemblyBam)/$(name)_index/
	mkdir $(fAssemblyBam)/$(name)_index
	STAR --runMode genomeGenerate --genomeDir $(fAssemblyBam)/$(name)_index --genomeFastaFiles $(word 2, $+) --runThreadN 8 --genomeSAindexNbases 6
	rm -f Log.out
	STAR \
        --chimSegmentMin        8 \
        --chimJunctionOverhangMin 8 \
        --alignSJoverhangMin 8 \
        --outSJfilterReads Unique \
        --outSJfilterOverhangMin 8 8 8 8 \
        --outSJfilterCountUniqueMin 2 2 2 2 \
        --outSJfilterCountTotalMin 2 2 2 2 \
        --outSJfilterDistToOtherSJmin 0 0 0 0 \
        --genomeDir $(fAssemblyBam)/$(name)_index \
        --runThreadN $(np) \
        --outReadsUnmapped unmapped \
        --outStd SAM \
        --outSAMmode Full \
        --outFileNamePrefix $(patsubst %.bam,%_,$@) \
        --readFilesCommand zcat \
        --readFilesIn $(word 1, $+) | samtools view -bS - \
        > $(fAssemblyBam)/$(name)_star_tmp.bam
	samtools sort $(fAssemblyBam)/$(name)_star_tmp.bam $(patsubst %.bam,%,$@)
	rm -rf $(fAssemblyBam)/$(name)_index
	rm -f $(fAssemblyBam)/$(name)_star_tmp.bam

$(fAssemblyBam)/%_star_Log.final.out: $(fAssemblyBam)/%_star.bam
	echo "Following dummy prerequisite for $@"


############################################################
# GET COVERAGE AND VARIANT REPORT FOR ALL TRIMMED SEGMENTS #
############################################################

# Coverage table
$(fAssembly)/%.info: $(fAssembly)/%.fa
	fasta-get-info.pl $< > $@

$(fAssembly)/%.fulllength.bed: $(fAssembly)/%.info
	$(awkt) '$$1!~/^#/{print $$1,0,$$2,$$1}' $< > $@

$(fAssembly)/%.fulllength.tmp: $(fAssemblyBam)/%_bwa.bam $(fAssembly)/%.fulllength.bed
	coverageBed -abam $< -b $(word 2, $+) | cut -f 1,5,6,8 > $@

$(fAssembly)/%.fulllength.cov: $(fAssembly)/%.fulllength.tmp
	add-header $< '#id' full_count full_bp_covered full_fraction_covered; mv -f $< $@

$(fAssembly)/%.info.cov: $(fAssembly)/%.info $(fAssembly)/%.fulllength.cov
	join-by-ids -a 1 -1 1 -2 1 $< $(word 2, $+) > $@;

# Report coverage and variants
$(fAssembly)/%.stats-coverage.txt: $(fAssemblyBam)/%_bwa.bam $(fAssembly)/%.fulllength.bed
	coverageBed -d -abam $< -b $(word 2, $+) | cut -f 4-6 > $@

$(fAssembly)/%.stats-DIP.txt: $(fAssemblyBam)/%_star.bam $(fAssembly)/%.stats-coverage.txt
	get-DIP-breakpoints-star.py $(patsubst %.bam,%_SJ.out.tab,$<) $(word 2, $+) $@

$(fAssembly)/%.stats-variants.txt: $(fAssemblyBam)/%_bwa.bam $(fAssembly)/%.fa
	influenza_count-variant-bases.pl -b $< -f $(word 2, $+) > $@

$(fAssembly)/%.stats-mapping.txt: $(fAssembly)/%.stats-DIP.txt $(fAssemblyBam)/%_star_Log.final.out $(fFilteredReads)/$(name)_cutadapt.log
	reads_report.py $(word 1, $+) $(word 2, $+) $(word 3, $+) $@

$(fAssembly)/%.report.pdf: $(fAssembly)/%.stats-coverage.txt $(fAssembly)/%.stats-variants.txt $(fAssembly)/%.stats-DIP.txt $(fAssembly)/%.stats-mapping.txt
	influenza_finalreport.R -l 100 -i $< -v $(word 2, $+) -d $(word 3, $+) -a $(word 4, $+) -o $(patsubst %.pdf,%,$@) -t $(varthr)


####################################
# GATHER FULLY ASSEMBLED FRAGMENTS #
####################################

$(fAssembly)/%_complete.ids: $(fAssembly)/%.fa
	grep -P '(complete\|contiguous)|(complete\|chimera)' $< | perl -pi -e 's/^>//' > $@

$(fAssembly)/%_complete.fa: $(fAssembly)/%.fa $(fAssembly)/%_complete.ids
	fasta-filter-by-id.pl -f $(word 1, $+) -m $(word 2, $+) > $@

$(fAssembly)/%_complete.info: $(fAssembly)/%_complete.fa
	fasta-get-info.pl $< > $@

$(fAssembly)/%_complete.check: $(fAssembly)/%.info.cov
	process_assembly.py --cov $< --out $@


##################
# PATCH VARIANTS #
##################

# If no curation was necessary we just go straight from the 'complete' segments to final report
03_assembly/%_curated.fa: 03_assembly/%_complete.fa
	cp $< $@

# BWA mapping (for read coverage and variant analysis)
$(fVarPatch)/%_bwa_index.amb: 03_assembly/%.fa
	bwa index -p $(patsubst %.amb,%,$@) $< 

$(fVarPatch)/%_bwa_tmp.bam: $(fVarPatch)/%_bwa_index.amb $(fFilteredReads)/$(name).cutadapt.fastq.gz
	bwa mem -t $(np) -M $(patsubst %.amb,%,$<) $(word 2, $+) | samtools view -S -bq 1 - > $@
	rm -f $(patsubst %.amb,%*,$<)

$(fVarPatch)/%_bwa.bam: $(fVarPatch)/%_bwa_tmp.bam
	samtools sort $< $(patsubst %.bam,%,$@)

# Patch variants based on bwa mpileup analysis
$(fVarPatch)/%.stats-variants.txt: $(fVarPatch)/%_bwa.bam 03_assembly/%.fa
	influenza_count-variant-bases.pl -b $< -f $(word 2, $+) > $@

$(fAssembly)/%_final.fa: 03_assembly/%_assembly_curated.fa $(fVarPatch)/%_assembly_curated.stats-variants.txt
	patch-variants-from-mpileup.pl -f $< -m $(word 2, $+) 2> $(fVarPatch)/$(name)_assembly_curated.varpatch.log > $@

# Make a variant call file
$(fAssembly)/%.variants.calls.txt: $(fAssembly)/%.stats-variants.txt
	${awkt} '($$3>=0.05 && $$2!=4) || $$1~/^#/' $< > $@


####################
# ANNOTATE GENOMES #
####################

$(fAssembly)/%.features_table.txt: $(fAssembly)/%.fa
	influenza_run-flu-annotation-tool.pl -i $<

$(fAssembly)/%_subtype.txt: $(fAssembly)/%.features_table.txt
	influenza_subtype-from-annotation.pl -i $< -o $(patsubst %_subtype.txt, %, $@)

############################################
# LOSEQ VARIANT CALLING (INCLUDING INDELS) #
############################################

# Call and filter first-pass variant sites
%_bwa_loseq_knownsites.vcf: %.fa %_bwa.bam %_bwa.bam.bai
	lofreq call-parallel --pp-threads $(np) -f $< -o $@ $(word 2, $+)
%_bwa_loseq_knownsites_filt.vcf: %_bwa_loseq_knownsites.vcf
	bcftools filter -e "AF<0.005" $< 2> /dev/null > $@

# Recalibrate base qualities using picard and GATK
%.dict: %.fa
	java -Xmx2g -jar $(PICARD_HOME)/CreateSequenceDictionary.jar R=$< O=$@
%_bwa_rg.bam: %_bwa.bam
	java -Xmx2g -jar $(PICARD_HOME)/AddOrReplaceReadGroups.jar I=$< O=$@ RGLB=A RGPL=illumina RGPU=run RGSM=$(patsubst %_bwa.bam,%,$<)
%_bwa_rg_recal.table: %.fa %_bwa_rg.bam %_bwa_rg.bam.bai %_bwa_loseq_knownsites_filt.vcf %.dict
	java -Xmx4g -jar $(GATK_JAR) -T BaseRecalibrator -R $< -I $(word 2, $+) --filter_reads_with_N_cigar -knownSites $(word 4, $+) -o $@
%_bwa_rg_bqsr.bam: %.fa %_bwa_rg.bam %_bwa_rg_recal.table %.dict
	java -jar $(GATK_JAR) -T PrintReads -R $< -I $(word 2, $+) --filter_reads_with_N_cigar -BQSR $(word 3, $+) -o $@

# Do final calling including indels
%_bwa_loseq_snvindel.vcf: %.fa %_bwa_rg_bqsr.bam %_bwa_rg_bqsr.bam.bai
	lofreq call-parallel --call-indels --pp-threads $(np) -f $< -o $@ $(word 2, $+)
