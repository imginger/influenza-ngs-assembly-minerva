#!/usr/bin/python
# -*- coding: utf-8 -*-

import _mysql
import sys
import bz2
from collections import defaultdict


def usage():
        return "usage:"+"\n"+"\tpyblob2.py <ids text file> <output fasta file>"

if len(sys.argv)!=3:
        print usage()
        sys.exit(0)

def getSequence(txtfile):

    f = open(txtfile, "r")
    lst = []
    isolateId = []
    extractId = []
    sequence1, sequence2, sequence3, sequence4, sequence5, sequence6, sequence7, sequence8 = ([] for z in range(8))
    seq1_header, seq2_header, seq3_header, seq4_header, seq5_header, seq6_header, seq7_header, seq8_header = ([] for y in range(8))

    for line in f:
        lst.append([str(x) for x in line.split()])
    col1 = [ x[0] for x in lst]
    len_col = len(col1)

    for i in range(0,len_col):
        con = _mysql.connect(host='db.hpc.mssm.edu', user='vanbah01_root', passwd='QUmqxVHqAcxUHFaZS', db='vanbah01_influenza')
        con.query("SELECT `Extracts`.`idExtract` from `Extracts` join `Isolates` on `Extracts`.`IsolateID` = `Isolates`.`id` WHERE `Isolates`.`Sample_Identifier` = '%s'" % col1[i])
        result = con.use_result()
        id_extract = result.fetch_row()[0]
        id_extract = int(''.join(id_extract))
        extractId.append(id_extract)
        con.close()

        for x in range(1,9):
            try:
                con = _mysql.connect(host='db.hpc.mssm.edu', user='vanbah01_root', passwd='QUmqxVHqAcxUHFaZS', db='vanbah01_influenza')
                con.query("SELECT `sequence` FROM `Sequences` WHERE `Segment`='%d' AND`ExtractID`='%s'" % (x, extractId[i]))
                result = con.use_result()
                if (x == 1):
                    seq1 = result.fetch_row()[0]
                    seq1 = ''.join(seq1)
                    seq1 = bz2.decompress(seq1).replace("\n","")
                    sequence1.append(seq1)
                    head1 = ">Sequence_identifier:'%s'.PB2|Sample_Identifier:'%s'|Segment:PB2" % (col1[i],col1[i])
                    seq1_header.append(head1)
                if (x == 2):
                    seq2 = result.fetch_row()[0]
                    seq2 = ''.join(seq2)
                    seq2 = bz2.decompress(seq2).replace("\n","")
                    sequence2.append(seq2)
                    head2 = ">Sequence_identifier:'%s'.PB1|Sample_Identifier:'%s'|Segment:PB1" % (col1[i],col1[i])
                    seq2_header.append(head2)
                if (x == 3):
                    seq3 = result.fetch_row()[0]
                    seq3 = ''.join(seq3)
                    seq3 = bz2.decompress(seq3).replace("\n","")
                    sequence3.append(seq3)
                    head3 = ">Sequence_identifier:'%s'.PA|Sample_Identifier:'%s'|Segment:PA" % (col1[i],col1[i])
                    seq3_header.append(head3)
                if (x == 4):
                    seq4 = result.fetch_row()[0]
                    seq4 = ''.join(seq4)
                    seq4 = bz2.decompress(seq4).replace("\n","")
                    sequence4.append(seq4)
                    head4 = ">Sequence_identifier:'%s'.HA|Sample_Identifier:'%s'|Segment:HA" % (col1[i],col1[i])
                    seq4_header.append(head4)
                if (x == 5):
                    seq5 = result.fetch_row()[0]
                    seq5 = ''.join(seq5)
                    seq5 = bz2.decompress(seq5).replace("\n","")
                    sequence5.append(seq5)
                    head5 = ">Sequence_identifier:'%s'.NP|Sample_Identifier:'%s'|Segment:NP" % (col1[i],col1[i])
                    seq5_header.append(head5)
                if (x == 6):
                    seq6 = result.fetch_row()[0]
                    seq6 = ''.join(seq6)
                    seq6 = bz2.decompress(seq6).replace("\n","")
                    sequence6.append(seq6)
                    head6 = ">Sequence_identifier:'%s'.NA|Sample_Identifier:'%s'|Segment:NA" % (col1[i],col1[i])
                    seq6_header.append(head6)
                if (x == 7):
                    seq7 = result.fetch_row()[0]
                    seq7 = ''.join(seq7)
                    seq7 = bz2.decompress(seq7).replace("\n","")
                    sequence7.append(seq7)
                    head7 = ">Sequence_identifier:'%s'.MP|Sample_Identifier:'%s'|Segment:MP" % (col1[i],col1[i])
                    seq7_header.append(head7)
                if (x == 8):
                    seq8 = result.fetch_row()[0]
                    seq8 = ''.join(seq8)
                    seq8 = bz2.decompress(seq8).replace("\n","")
                    sequence8.append(seq8)
                    head8 = ">Sequence_identifier:'%s'.NS|Sample_Identifier:'%s'|Segment:NS" % (col1[i],col1[i])
                    seq8_header.append(head8)
            except:
                print "no segment %d for %s" % (x, extractId[i])
            finally:
                if con:
                    con.close()  
   
 
    res_dict1 = dict(zip(seq1_header,sequence1))
    res_dict2 = dict(zip(seq2_header,sequence2))
    res_dict3 = dict(zip(seq3_header,sequence3))
    res_dict4 = dict(zip(seq4_header,sequence4))
    res_dict5 = dict(zip(seq5_header,sequence5))
    res_dict6 = dict(zip(seq6_header,sequence6))
    res_dict7 = dict(zip(seq7_header,sequence7))
    res_dict8 = dict(zip(seq8_header,sequence8))

    dicts = [res_dict1, res_dict2, res_dict3, res_dict4, res_dict5, res_dict6, res_dict7, res_dict8]
    super_dict = defaultdict(set)
    for d in dicts:
        for k, v in d.iteritems():
            super_dict[k].add(v)
    

    f.close()
    return super_dict


def createFile(txtfile, outfile):
    fle=open(outfile,"w")
    mydict = getSequence(txtfile)
    all_headers = mydict.keys()
    all_headers = sorted(all_headers)

    for m in range(0, len(mydict)):
        header = all_headers[m]
        se = str(mydict.get(header)).lstrip("set(['").rstrip("'])")
        fle.write(header + "\n")
        fle.write(se + "\n")

    fle.close()
    return fle


if __name__ == "__main__":

    txtfile = sys.argv[1]
    outfile = sys.argv[2]

    try:
        createFile(txtfile, outfile)
    except:
        print "Error reading text file\n"
        print usage()
        sys.exit(0)
