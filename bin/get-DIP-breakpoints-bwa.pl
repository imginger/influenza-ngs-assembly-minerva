#!/usr/bin/perl

# 02.01.2015 14:03:38 EST
# Harm van Bakel <hvbakel@gmail.com>

# MODULES
use strict;
use warnings;
use Getopt::Long;

# GET PARAMETERS
my $sHelp        = 0;
GetOptions("help!"   => \$sHelp);

# PRINT HELP
$sHelp = 1 unless(@ARGV>0);
if ($sHelp) {
   my $sScriptName = ($0 =~ /^.*\/(.+$)/) ? $1 : $0;
   die <<HELP

   Usage: $sScriptName <bam-file>
   
   Extract DIP breakpoint counts from a BWA bam alignment file
   
   Arguments: 
    -help
      This help message
   
HELP
}


##########
## MAIN ##
##########

my %hStats;
my $sHitID = '';
my %hHitSeg;
my %hHitStr;
my @anHits;
open IN, "bamToBed -i $ARGV[0] | sort -t '\t' -k4,4 -k2,2n |" or die "Error: can't run bamToBed: $!\n";
while (<IN>){
   next if (/^\s*$/);
   next if (/^ *#/);
   s/[\n\r]+$//;
   my ($sSeg, $nStart, $nEnd, $sReadID, $nScore, $sStrand) = split /\t/, $_, -1;
   if ($sReadID ne $sHitID ){
      if (scalar(@anHits)==2 and scalar(keys %hHitSeg)==1 and scalar(keys %hHitStr)==1){
         my $sHitSeg = (keys %hHitSeg)[0];
         my $sHitStr = (keys %hHitStr)[0];
         my $sKey = join("\t", $sHitSeg, $anHits[0][1], $anHits[1][0], $sHitStr);
         $hStats{$sKey}++;
      }
      $sHitID  = $sReadID;
      %hHitSeg = ();
      %hHitStr = ();
      @anHits  = ();
   }
   $hHitSeg{$sSeg}++;
   $hHitStr{$sStrand}++;
   push @anHits, [($nStart, $nEnd)];
}
close IN;

# Print breakpoint list
foreach my $sChimera (keys %hStats){
   print join("\t", $sChimera, $hStats{$sChimera}), "\n";
}
