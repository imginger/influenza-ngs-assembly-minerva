#!/usr/bin/perl

# 05.10.2015 12:04:33 EDT
# Harm van Bakel <hvbakel@gmail.com>

# MODULES
use strict;
use warnings;
use Getopt::Long;

# GET PARAMETERS
my $sHelp        = 0;
my $sMpileupFile = "";
my $sFastaFile   = "";
my $nThreshold   = 0.5;
GetOptions("help!"       => \$sHelp,
           "mpileup:s"   => \$sMpileupFile,
           "fasta:s"     => \$sFastaFile,
           "threshold:s" => \$nThreshold);

# PRINT HELP
$sHelp = 1 unless($sMpileupFile and $sFastaFile);
if ($sHelp) {
   my $sScriptName = ($0 =~ /^.*\/(.+$)/) ? $1 : $0;
   die <<HELP

   Usage: $sScriptName
   
   Apply variants found in mpileup analysis to a fasta file.
   
   Arguments:
    -f --fasta <string>
      File with sequences in fasta format
    -m --mpileup <string>
      File with mpileup output
    -t --threshold <number>
      Variant frequency threshold. Default: $nThreshold
    -help
      This help message
   
HELP
}

##########
## MAIN ##
##########

# Check arguments
die "Error: file '$sFastaFile' does not exist\n"   unless (-e $sFastaFile);
die "Error: file '$sMpileupFile' does not exist\n" unless (-e $sMpileupFile);
die "Error: Threshold must be between 0-1\n" unless ($nThreshold =~ /^\d{1}\.?\d?$/);

# Read sequences
my $rhSequences = read_fasta_to_hash($sFastaFile);

# Read variants
my $rhVariants  = read_mpileup_variants($sMpileupFile, $nThreshold);

# Apply variants to sequences
my $nFaLineSize = 100;
foreach my $sID (sort keys %$rhSequences){
   my $sSeq = $rhSequences->{$sID};
   if (exists $rhVariants->{$sID} ){
      foreach my $nPos (sort {$a <=> $b} keys %{$rhVariants->{$sID}}){
         my $sRefBase = $rhVariants->{$sID}{$nPos}{ref};
         my $sAltBase = $rhVariants->{$sID}{$nPos}{alt};
      
         # Make sure the reference base matches the base found in the sequence
         my $sObsRefBase = substr($sSeq, $nPos, 1);
         if ($sRefBase eq  $sObsRefBase){
            warn "Replacing '$sRefBase' by '$sAltBase' at position $nPos in '$sID'\n";
            substr($sSeq, $nPos, 1) = $sAltBase;
         }
         else{
            die "Error: was expecting a '$sRefBase' at position $nPos in '$sID' but got '$sObsRefBase'\n";
         }
      }
   }
   $sSeq =~ s/.{$nFaLineSize}/$&\n/sg;
   $sSeq =~ s/\n+$//;
   print ">$sID\n$sSeq\n";
}


#################
## SUBROUTINES ##
#################

# read_fasta_to_hash
#
# Read a multisequence fasta file into a hash structure
sub read_fasta_to_hash {
   my ($sFastaFile) = @_;

   my %hSequences;
   my $sFastaHeader = '';
   my $sFastaSeq    = '';
   open FASTA, "<$sFastaFile" or die "Error: can't open '$sFastaFile': $!\n";
   while (<FASTA>){
      next if (/^\s*$/);
      next if (/^ *#/);
      s/[\n\r]+$//;
      if (/^>/ or eof){
         if (eof){
            die "Error: file ends in fasta header without sequence\n" if (/^>/);
            $sFastaSeq .= $_;
         }
         if ($sFastaHeader){
            if (exists $hSequences{$sFastaHeader}){
               die "Error: Duplicate sequence IDs found: '$sFastaHeader'\n";
            }
            else{
               $hSequences{$sFastaHeader} = $sFastaSeq;
            }
         }
         $sFastaHeader = $_;
         $sFastaHeader =~ s/^>\s*//;
         $sFastaHeader =~ s/ .*$//;
         $sFastaSeq    = "";
      }
      else{
         $sFastaSeq .= $_ if ($sFastaHeader);
      }
   }
   close FASTA;
   return \%hSequences;
}

# read_mpileup_variants
#
# Read variants from mpileup file
sub read_mpileup_variants {
   my ($sMpileupFile, $nThreshold) = @_;
   my %hDupTracking;
   my %hVariants;

   open MPILEUP, "<$sMpileupFile" or die "Error: can't open '$sMpileupFile': $!\n";
   my $sHeader = <MPILEUP>;
   die "Error: '$sMpileupFile' is not in the expected format\n" unless ($sHeader eq "#id\tpos\tAltRatio\tRefBase\tReadSum\tRefFwd\tRefRev\tAfwd\tArev\tCfwd\tCrev\tGfwd\tGrev\tTfwd\tTrev\n");
   while (<MPILEUP>){
      next if (/^\s*$/);
      next if (/^ *#/);
      s/[\n\r]+$//;
      my ($sID, $nPos, $nAltRatio, $sRefBase, $nReadSum, $nRefFwd, $nRefRev, $nAfwd, $nArev, $nCfwd, $nCrev, $nGfwd, $nGrev, $nTfwd, $nTrev) = split /\t/;
      
      # Make the position zero-based
      $nPos--;
      
      # Make sure there are no duplicates in the variant file
      die "Error: duplicant variant mapping found for '$sID', position $nPos\n" if (exists $hDupTracking{$sID}{$nPos});
      $hDupTracking{$sID}{$nPos}++;
      
      # Gather reference and alternative frequencies
      my %hFrequencies;
      if ($nReadSum){
         %hFrequencies = (R => ($nRefFwd + $nRefRev) / $nReadSum,
                          A => ($nAfwd + $nArev) / $nReadSum,
                          C => ($nCfwd + $nCrev) / $nReadSum,
                          G => ($nGfwd + $nGrev) / $nReadSum,
                          T => ($nTfwd + $nTrev) / $nReadSum);
      }
      else{
         %hFrequencies = (R => 0,
                          A => 0,
                          C => 0,
                          G => 0,
                          T => 0);
      }
      
      # Find out how many alternative alleles have a frequency greater than the threshold and the reference base
      my ($nAltCount, $sTopAltBase, $nTopAltFreq) = (0, "", 0);
      foreach my $sAltBase ('A','C','G','T'){
         if ( ($hFrequencies{$sAltBase} > $hFrequencies{R}) and ($hFrequencies{$sAltBase} > $nThreshold) ){
            #print join("\t", $sID, $nPos, $sAltBase, $hFrequencies{$sAltBase}, $hFrequencies{R}, $hFrequencies{$sAltBase}, $nThreshold), "\n";
            $nAltCount++;
            if ($hFrequencies{$sAltBase} > $nTopAltFreq){
               $sTopAltBase = $sAltBase;
               $nTopAltFreq = $hFrequencies{$sAltBase};
            }
         }
      }
      
      # Store variants
      if ($nAltCount > 0){
         $hVariants{$sID}{$nPos}{ref} = $sRefBase;
         $hVariants{$sID}{$nPos}{alt} = $sTopAltBase;
      }
      warn ("Warning: multiple alternative alleles found at threshold $nThreshold. Selecting most frequent allele\n") if ($nAltCount > 1);
   }
   close MPILEUP;
   return \%hVariants;
}
