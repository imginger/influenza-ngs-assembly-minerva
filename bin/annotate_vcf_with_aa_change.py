#!/usr/bin/env python 
__author__ = 'luiscunha'

import re, sys

p = re.compile('^#')
q = re.compile('^>')


class TranslateSequence:
    """Translate the given DNA sequence to protein sequence
   """

    def __init__(self):
        self.codon_table = {"AAA": "K", "AAC": "N", "AAG": "K", "AAT": "N", "ACA": "T", "ACC": "T", "ACG": "T",
                            "ACT": "T", "AGA": "R", "AGC": "S", "AGG": "R", "AGT": "S", "ATA": "I", "ATC": "I",
                            "ATG": "M", "ATT": "I", "CAA": "Q", "CAC": "H", "CAG": "Q", "CAT": "H", "CCA": "P",
                            "CCC": "P", "CCG": "P", "CCT": "P", "CGA": "R", "CGC": "R", "CGG": "R", "CGT": "R",
                            "CTA": "L", "CTC": "L", "CTG": "L", "CTT": "L", "GAA": "E", "GAC": "D", "GAG": "E",
                            "GAT": "D", "GCA": "A", "GCC": "A", "GCG": "A", "GCT": "A", "GGA": "G", "GGC": "G",
                            "GGG": "G", "GGT": "G", "GTA": "V", "GTC": "V", "GTG": "V", "GTT": "V", "TAA": "STOP",
                            "TAC": "Y", "TAG": "STOP", "TAT": "Y", "TCA": "S", "TCC": "S", "TCG": "S", "TCT": "S",
                            "TGA": "STOP", "TGC": "C", "TGG": "W", "TGT": "C", "TTA": "L", "TTC": "F", "TTG": "L",
                            "TTT": "F"}

    def translate(self, sequence_):
        g = sequence_
        splitted_g = [g[0 + i:3 + i] for i in range(0, len(g), 3)]
        if len(splitted_g[-1]) != 3:
            splitted_g = splitted_g[:-1]
        trans = [self.codon_table[c] for c in splitted_g]
        return "".join(trans)


def getFeature(basename):
    features = {}
    genes = {}
    with open(basename + ".anno", "r") as f:
        line = f.readline()
        chrom = line.split()[1].split("|")[0]
        l = []
        for line in f:
            if q.match(line):
                features[chrom] = l
                chrom = line.split()[1].split("|")[0]
                l = []
            else:
                l.append(line.strip())
        features[chrom] = l
    #print features

    for chrom in [1, 3, 4, 5, 6]:
        for line in features[str(chrom)]:
            if "gene" in line and len(line.split()) == 3:
                start = line.split()[0]
                end = line.split()[1]
                genes[chrom] = (int(start), int(end))
                break
    genes[2] = []
    genes[7] = []
    genes[8] = []
    for line in features["2"]:
        if "gene" in line and len(line.split()) == 3:
            genes[2].append((int(line.split()[0]), int(line.split()[1])))

    for i in range(len(features["7"])):
        line = features["7"][i]
        if "CDS" in line and len(line.split()) == 3:
            genes[7].append((int(line.split()[0]), int(line.split()[1])))
            l1 = features["7"][i + 1]
            if len(l1.split()) == 2:
                try:
                    start = int(l1.split()[0])
                    end = int(l1.split()[1])
                    genes[7].append((start, end))
                except:
                    pass

    for i in range(len(features["8"])):
        line = features["8"][i]
        if "CDS" in line and len(line.split()) == 3:
            genes[8].append((int(line.split()[0]), int(line.split()[1])))
            l1 = features["8"][i + 1]
            if len(l1.split()) == 2:
                try:
                    start = int(l1.split()[0])
                    end = int(l1.split()[1])
                    genes[8].append((start, end))
                except:
                    pass
    return genes


def getFasta(basename):
    genome = {}
    with open(basename + "_curated.fa", "r") as f:
        line = f.readline()
        chrom = line[1]
        genome[chrom] = []
        seq = ""
        for line in f:
            if ">" in line:
                genome[chrom] = seq
                chrom = line[1]
                seq = ""
            else:
                seq += line.strip()
        genome[chrom] = seq
    return genome


def getVariant(chrom, pos, ref, alt, genes, fasta):
    print "****************"
    t = TranslateSequence()
    seq = genes[int(chrom)]
    print "seq ", seq, chrom, pos
    """Segments 1, 3, 4, 5, 6
    """
    if int(chrom) in [1, 3, 4, 5, 6]:
        if int(pos) < int(seq[0]) or int(pos) > int(seq[1]):  # if mutation outside the cds of this protein, skip
            print "skipping seg " + str(chrom)
            return {}

        proteinsNames = {1: "PB2", 3: "PA", 4: "HA", 5: "NP", 6: "NA"}
        sequence = fasta[chrom][seq[0] - 1:seq[1]]
        variant = sequence[int(pos) - int(seq[0])]
        varpos = int(pos) - int(seq[0] - 1)
        codonNumber = varpos / 3
        codon = "".join(list(sequence[((varpos-1) / 3) * 3:((varpos-1) / 3 * 3 + 3)])).upper()
        newcodon = list(sequence[((varpos-1) / 3) * 3:((varpos-1) / 3 * 3 + 3)])
        var_codon_pos = varpos % 3
        newcodon[var_codon_pos - 1] = alt
        newcodon = "".join(newcodon).upper()
        print seq, variant, varpos, codon, newcodon, var_codon_pos, t.translate(codon), t.translate(newcodon)
        return {proteinsNames[int(chrom)]: (codonNumber, (t.translate(codon), t.translate(newcodon)))}
        """Segment 2
        """
    elif int(chrom) == 2:
        if len(seq) != 2:
            print "WARNING: check segment two that it contains both PB1 and PB1-F2"
        else:
            proteins = {}
            for cdna in seq:
                print "cdna: ", cdna
                if int(cdna[0]) < 75:
                    proteinName = "PB1"
                else:
                    proteinName = "PB1-F2"
                if int(pos) < int(cdna[0]) or int(pos) > int(cdna[1]):  # if mutation outside the cds of this protein, skip
                    print pos, cdna
                    print "skipping" + proteinName
                    continue
                print "prot: ", proteinName
                sequence = fasta[chrom][cdna[0] - 1:cdna[1]]
                variant = sequence[int(pos) - int(cdna[0])]
                varpos = int(pos) - int(cdna[0] - 1)
                codonNumber = varpos / 3
                codon = "".join(list(sequence[((varpos-1) / 3) * 3:((varpos-1) / 3 * 3 + 3)])).upper()
                newcodon = list(sequence[((varpos-1) / 3) * 3:((varpos-1) / 3 * 3 + 3)])
                var_codon_pos = varpos % 3
                newcodon[var_codon_pos - 1] = alt
                newcodon = "".join(newcodon).upper()
                print(cdna, variant, varpos, codon, newcodon, var_codon_pos, t.translate(codon), t.translate(newcodon))
                proteins[proteinName] = (codonNumber, (t.translate(codon), t.translate(newcodon)))
            print "p: ", proteins
            return proteins
            """Segments 7,8
            """
    elif int(chrom) == 7 or int(chrom) == 8:
        if len(seq) != 3:
            print "WARNING: check segment " + str(chrom) + " that it contains both proteins coded by the segment"
        else:
            proteins = {}
            seq = [[seq[0]], [seq[1], seq[2]]]
            for cdna in seq:
                print "cdna: ", cdna
                if int(chrom) == 7:
                     if int(cdna[0][1]) > 200:
                        proteinName = "M1"
                     else:
                        proteinName = "M2"
                elif int(chrom) == 8:
                    if int(cdna[0][1]) > 200:
                        proteinName = "NS1"
                    else:
                        proteinName = "NEP"
                    print "prot: ", proteinName

                if len(cdna) == 1:
                    print "-+-+-+-+-+-+-+-+-+-+-+-+-"
                    print "len(seq) == 1"
                    if int(pos) < int(cdna[0][0]) or int(pos) > int(cdna[0][1]):  # if mutation outside the cds of this protein, skip
                        print chrom, pos, cdna
                        print "skipping"
                        continue
                    sequence = fasta[chrom][cdna[0][0]-1:cdna[0][1]]
                    variant = sequence[int(pos) - int(cdna[0][0])]
                    varpos = int(pos) - int(cdna[0][0]) + 1 # 1 based sequence
                    varpos = int(pos) - int(cdna[0][0]) #0 based sequence array
                    codonNumber = varpos / 3
                    codon = "".join(list(sequence[((varpos) / 3) * 3:((varpos) / 3 * 3 + 3)])).upper()
                    newcodon = list(sequence[((varpos) / 3) * 3:((varpos) / 3 * 3 + 3)])
                    var_codon_pos = varpos % 3  #(0,1,2)
                    newcodon[var_codon_pos] = alt
                    newcodon = "".join(newcodon).upper()

                    print(cdna, variant, varpos, codonNumber, codon, newcodon, var_codon_pos, t.translate(codon), t.translate(newcodon), sequence[(varpos / 3) * 3:(varpos / 3 * 3 + 3)])
                    proteins[proteinName] = (codonNumber, (t.translate(codon), t.translate(newcodon)))
                else:
                    print("-+-+-+-+-+-+-+-+-+-+-+-+-")
                    print("len(x) == 2")
                    #Position is 1 indexed, cdna[][] is 0-indexed, therefore must correct pos by 1
                    if int(pos) < int(cdna[0][0]) or int(pos) > int(cdna[1][1]) or (int(pos) > int(cdna[0][1]) and int(pos) < cdna[1][0]):  # if mutation outside the cds of this protein, skip
                        print chrom, pos, cdna
                        print "skipping"
                        continue
                    sequence = fasta[chrom][cdna[0][0] - 1:cdna[0][1]] + fasta[chrom][cdna[1][0] - 1:cdna[1][1]]
                    ##Position is 1 indexed, cdna[][] is 0-indexed, therefore must correct pos by 1
                    if int(pos)-1 < int(cdna[0][1]):
                        variant = sequence[int(pos) - int(cdna[0][0])]
                        varpos = int(pos) - int(cdna[0][0]) + 1 # 1 based sequence
                        varpos = int(pos) - int(cdna[0][0]) #0 based sequence array
                        var_codon_pos = (varpos) % 3
                        codonNumber = varpos / 3
                        print "c1#, ", varpos, codonNumber
                        codon = "".join(list(sequence[codonNumber * 3:codonNumber * 3 + 3])).upper()
                        print "c: ", varpos, codon, codonNumber
                        newcodon = list(sequence[codonNumber * 3:codonNumber * 3 + 3])
                        print "var1 ", variant
                    else:
                        variant = sequence[int(pos) - int(cdna[0][0]) - (cdna[1][0]-cdna[0][1])+1]
                        varpos = int(pos) - int(cdna[0][0]) - (cdna[1][0]-cdna[0][1]) + 1
                        var_codon_pos = (varpos) % 3
                        codonNumber = varpos / 3
                        print "c1#, ", varpos, codonNumber
                        codon = "".join(list(sequence[codonNumber * 3:codonNumber * 3 + 3])).upper()
                        print "c: ", varpos, codon, codonNumber
                        newcodon = list(sequence[codonNumber * 3:codonNumber * 3 + 3])
                        print "var2 ", variant



                    newcodon[var_codon_pos] = alt
                    newcodon = "".join(newcodon).upper()
                    print cdna, variant, varpos, codonNumber, codon, newcodon, var_codon_pos, t.translate(codon), t.translate(newcodon)
                    proteins[proteinName] = (codonNumber, (t.translate(codon), t.translate(newcodon)))
        print "p: ", proteins
        print "------------------------"
        return proteins


def readloseq(basename, genes, fasta):
    annotations = {}
    with open(basename + "_curated_bwa_loseq_snvindel.vcf", "r") as f:
        for line in f:
            if p.match(line):
                pass
            else:
                l = line.strip().split()
                info = l[7].split(";")
                for x in info:
                    if "AF" in x:
                        AF = float(x.split("=")[1])
                        if AF > 0.01 and "INDEL" not in info:
                            chrom = l[0][0]
                            pos = str(l[1])
                            ref = l[3]
                            alt = l[4]
                            refalt = ref + "_" + alt
                            if chrom not in annotations:
                                annotations[chrom] = {}
                            if pos not in annotations[chrom]:
                                annotations[chrom][pos] = {}
                                annotations[chrom][pos][refalt] = []
                            print(chrom, pos, ref, alt)

                            result = getVariant(chrom, pos, ref, alt, genes, fasta)
                            annotations[chrom][pos][refalt] = result
    return annotations


def write_output(basename, annotations):
    output = open(basename + "_curated_bwa_loseq_snvindel_annotated.vcf", "a")
    with open(basename + "_curated_bwa_loseq_snvindel.vcf", "r") as vcf_file_handler:
        filter_flag = 0
        for line in vcf_file_handler:
            if "##FILTER" in line and filter_flag == 0:
                s = '##INFO=<ID=AC,Number=1,Type=String,Description="<protein_name> <aa number><wt_aa>:<mut_aa">'
                output.write(s)
                output.write("\n")
                filter_flag = 1
            segment = line[0]
            if str(segment) in annotations:
                pos = line.split()[1]
                ref = line.split()[3]
                alt = line.split()[4]
                refalt = ref + "_" + alt
                if str(pos) in annotations[segment]:
                    if refalt in annotations[segment][pos]:
                        print "Seg", segment, pos, annotations[segment][pos][refalt]
                        if len(annotations[segment][pos][refalt]) != 0:
                            line = line.strip() + ";AC="
                            for x in annotations[segment][pos][refalt]:
                                line += x + " " + str(annotations[segment][pos][refalt][x][0]) + annotations[segment][pos][refalt][x][1][
                                    0] + ":" + annotations[segment][pos][refalt][x][1][1] + ","
                            if line[-1] == ",":
                                line = line[:-1]
                            line += "\n"
                            # pop out the annotation we just processed
                            #annotations[segment][pos] = annotations[segment][pos][refalt][1:]
                            output.write(line)
                        else:
                            output.write(line)
                    else:
                        output.write(line)
                else:
                    output.write(line)
            else:
                output.write(line)


if __name__ == "__main__":
    #basename = "A4_CTCAGA_L002_R1_001.HH3WFADXX"
    basename = sys.argv[1]
    fasta = getFasta(basename)
    genes = getFeature(basename)
    annotations = readloseq(basename, genes, fasta)
    #print annotations
    write_output(basename, annotations)

# print genes
#    print fasta['6']