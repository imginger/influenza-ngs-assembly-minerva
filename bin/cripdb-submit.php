#!/usr/bin/env php
<?php
$options = array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8',
                 PDO::FETCH_ASSOC);

// Read db configuration and set up connection
$db_config = getenv("HOME")."/.my.db.php.conf";
if(!file_exists($db_config)) {
   echo 'Please provide db configuration file in your homne directory named .my.db.php.conf with the following line (edit values):';
   echo "\n";
   echo 'a:4:{s:4:"host";s:15:"hostname";s:4:"user";s:13:"username";s:5:"passw";s:17:"password";s:2:"db";s:18:"databasename";}';
   echo "\n";
   die();
}
$db_param = unserialize(fread(fopen($db_config, "r"), filesize($db_config)));
$dbh      = new \PDO('mysql:host='.$db_param["host"].';dbname='.$db_param["db"], $db_param['user'], $db_param['passw'], $options);
$dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    
// Check arguments
if (!isset($argv[1])) {
   echo "usage: cripdb_submit.php <SampleID>\n";
   die();
}

/************************/
/* READ FASTA SEQUENCES */
/************************/

//read the fasta sequence and build a hash of the segment's sequences 
$filename  = $argv[1]."_curated_varpatch.fa";
if(!file_exists($filename)) die("Extract does not exist - check name\n");

// read and encode intact fasta file to upload to 'assemblies' table
$fasta = base64_encode(fread(fopen($filename, "r"), filesize($filename)));

// open fasta file to process each segment to upload to 'Sequences' table
$handle = fopen($filename, "r");
$complete=1;
if ($handle) {
   $count    =array();
   $segments =array();
   $sequence ="";
   $line     = fgets($handle);
   
   // Process the fasta header
   if (strpos($line, ">")>-1){
      if(!strpos($line, "complete")){
         $complete=0;
      }
      
      // Keep a count of how many times we encounter each segment
      if (array_key_exists(intval($line[1]), $count)){
         $count[intval($line[1])]+=1;
      }
      else {
         $count[intval($line[1])]=1;
      }

      $segment = $line[1];
      $info    = explode("|", $line);
      $segments[$segment]=array();
      $segments[$segment][$count[$segment]]=array("Type"=>$info[1], "Status"=>$info[2], "Chimeric"=>$info[3], "AssemblerID"=>$info[4]);
      $sequence="";
   }

   // Process the fasta sequence
   while (($line = fgets($handle)) !== false) {
      if (strpos($line, ">")>-1){
         $segments[$segment][$count[$segment]]['Sequence'] = bzcompress($sequence,9);
         $sequence = "";
         if(!strpos($line, "complete")){
            $complete=0;
         }
         
         if (array_key_exists(intval($line[1]), $count)){
            $count[intval($line[1])]+=1;
         }
         else {
            $count[intval($line[1])]=1;
         }
   
         $segment  =$line[1];
         $info=explode("|", $line);
         $segments[$segment][$count[$segment]]=array("Type"=>$info[1], "Status"=>$info[2], "Chimeric"=>$info[3], "AssemblerID"=>$info[4]);
      }
      else {
         $sequence=$sequence.$line;
      }
   }
   $segments[$segment][$count[$segment]]['Sequence']=bzcompress($sequence,9);

   // check that there are 8 segments present, otherwise set complete flag to false (0)
   if(count($count)<8){
      $complete=0;
      echo "Sample " . $argv[1] . " - partial assembly found\n";
   }
   else{
      echo "Sample " . $argv[1] . " - complete assembly found\n";
   }
   fclose($handle);
}
else {
   die("error opening the file.");
} 
	
/***************************/
/* PROCESS SEQUENCES TABLE */
/***************************/

// Get extractID of the sample (from joined Isolates + Extracts view)
$query     = "SELECT idExtract FROM `ExtractsIsolates` WHERE `Sample_Identifier` = '" . $argv[1] ."'" ;
$sth       = $dbh->prepare($query);
$sth->execute();
$result    = $sth->fetch(PDO::FETCH_ASSOC);
if (empty($result)){
   die("Error: could not find an extract with sample name " . $argv[1] . " in CRIPdb\n");
}
$extractID = intval($result["idExtract"]);
echo "Sample " . $argv[1] . " - maps to extract ID " . $extractID . "\nUpdating Sequence table\n";

// Get isolateID of the sample (from joined Isolates + Extracts view)
$query     = "SELECT IsolateID FROM `ExtractsIsolates` WHERE `Sample_Identifier` = '" . $argv[1] ."'" ;
$sth       = $dbh->prepare($query);
$sth->execute();
$result    = $sth->fetch(PDO::FETCH_ASSOC);
if (empty($result)){
   die("Error: could not find an isolate with sample name " . $argv[1] . " in CRIPdb\n");
}
$isolateID = intval($result["IsolateID"]);

// Delete any existing sequence data for this isolate from the database
$query = "Select * from `Sequences` WHERE `ExtractID`=". $extractID;
$sth   = $dbh->prepare($query);
$sth->execute();
if ($row=$sth->fetchall(PDO::FETCH_ASSOC)) {
   $query = "DELETE FROM `vanbah01_influenza`.`Sequences` WHERE `ExtractID`=" . $extractID; 
   $sth   = $dbh->prepare($query);
   $sth->execute();
}

// Now insert new sequence data
foreach($segments as $key=>$val0){
   foreach($val0 as $val){
      $query="INSERT INTO `vanbah01_influenza`.`Sequences` (`id`, `ExtractID`, `Segment`, `Type`, `Status`, `Chimeric`, `AssemblyID`, `Sequence`, `Timestamp`) VALUES (NULL, ?, ?, ?, ?, ?, ?, ?, CURRENT_TIMESTAMP);";
      $sth=$dbh->prepare($query);
      $sth->bindParam(1,$extractID);
      $sth->bindParam(2,$key);
      $sth->bindParam(3,$val["Type"]);
      $sth->bindParam(4,$val["Status"]);
      $sth->bindParam(5,$val["Chimeric"]);
      $sth->bindParam(6,$val["AssemblerID"]);
      $sth->bindParam(7,$val["Sequence"]);
      $ro=$sth->execute();
   }
}

/****************************/
/* PROCESS ASSEMBLIES TABLE */
/****************************/

// Encode the pdf report file
$filename    = $argv[1]."_curated_varpatch.report.pdf";
$report      = base64_encode(fread(fopen($filename, "r"),filesize($filename)));

// Encode the variant call file
$filename    = $argv[1]."_curated_varpatch.variants.calls.txt";
$variants    = base64_encode(fread(fopen($filename, "rb"),filesize($filename)));

// Encode the protein translations
$filename    = $argv[1]."_curated_varpatch_protein.fa";
$protein     = base64_encode(fread(fopen($filename, "rb"),filesize($filename)));

// Encode the annotation file
$filename    = $argv[1]."_curated_varpatch.anno";
$annotations = base64_encode(fread(fopen($filename, "rb"),filesize($filename)));

// Update or insert data in assemblies table
$query = "Select `idAssemblies` from `Assemblies` WHERE ExtractID LIKE ". $extractID  ;
$sth   = $dbh->prepare($query);
$ro    = $sth->execute();
if ($row=$sth->fetch()) {
   $query = "UPDATE `vanbah01_influenza`.`Assemblies` SET `Fasta`='".$fasta."', `proteinFasta`='".$protein."', `annotations`='".$annotations."', `PDFReport`='".$report."', `Variants`='".$variants."', `Complete`=".$complete.", `Timestamp`=CURRENT_TIMESTAMP  WHERE ExtractID=". $extractID;
   $sth   = $dbh->prepare($query);
   $ro    = $sth->execute();
	echo "Updating Assembly table\n";
}
else {
   $dbh->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING );
   $query = "INSERT INTO `vanbah01_influenza`.`Assemblies` (`idAssemblies`, `ExtractID`,`Fasta`, `proteinFasta`, `annotations`, `PDFReport`,`Variants`,`Complete`, `TimeStamp`) VALUES (NULL,".$extractID.",'".$fasta."','".$protein."','".$annotations."','".$report."','".$variants."',".$complete.",CURRENT_TIMESTAMP)";
   $sth   = $dbh->prepare($query);
   $ro    = $sth->execute();
   if (!$ro) print_r($sth->errorInfo());
   echo "Inserting into Assembly table\n";
}


/***********************************/
/* UPDATE STATUS IN EXTRACTS TABLE */
/***********************************/

// Define status based on whether we got complete sequences for all segments
if ($complete==0){
   $status = "Partial";
}
else {
   $status = "Complete";
}

//set subtype in extracts table according to the Segment 4 H type and segment 6 N type:
$query = "SELECT * from `Extracts` WHERE idExtract LIKE ". $extractID;
$sth   = $dbh->prepare($query);
$sth->execute();

if ($row=$sth->fetch(PDO::FETCH_ASSOC)) {
   // Determine HA type
   $subtype="";
   if (array_key_exists(4, $segments)){
      $h1      = explode("N", $segments[4][1]["Type"]);
      $h2      = explode("H", $h1[0]);
      $subtype = $subtype . "H" . $h2[1];
   }
	else {
      $subtype = $subtype."na";
	}
   
   // Determine NA type
   if (array_key_exists(6,$segments)){
      $n1      = explode("N", $segments[6][1]["Type"]);
      $subtype = $subtype."N" . $n1[1];
   }
	else {
      $subtype= $subtype . "na";
   }
   
   
   // Update the extracts table
   echo "Updating Extract table subtype with type ".$subtype .": " . $ro . "\n";
   $query = "UPDATE `vanbah01_influenza`.`Extracts` SET `Status` = ?, `Subtype`=?, Modified=CURRENT_TIMESTAMP WHERE `idExtract` = ". $extractID;
   $sth   = $dbh->prepare($query);
   $sth->bindParam(1,$status);
   $sth->bindParam(2,$subtype);
   $ro    = $sth->execute();

   // Update the isolates table with the same subtype as in Extracts table
   echo "Updating Isolates table subtype with type ".$subtype .": " . $ro . "\n";
   $query = "UPDATE `vanbah01_influenza`.`Isolates` SET `Subtype`=?, Modified=CURRENT_TIMESTAMP WHERE `id` = ". $isolateID;
   $sth   = $dbh->prepare($query);
   $sth->bindParam(1,$subtype);
   $ro    = $sth->execute();
}

// Disconnect from database
$dbh=null;

