#!/bin/sh

# 16.05.2013 11:31:03 EDT
# Harm van Bakel <hvbakel@gmail.com>

# Check arguments
if [ $# -ne 2 ]
then
  echo "Usage: `basename $0` <nmer-file> <influenza-reference>"
  exit 0
fi

# Count degenerate occurrences using agrep
rm -f $1.counts
for fwdmer in `cat $1`
do
   revmer=`echo -n $fwdmer | tr "acgtumrykvhdbnACGTUMRYKVHDBN" "tgcaakyrmbdhvxTGCAAKYRMBDHVX" | rev`
   count_fwd=`agrep -c -s -V0 -2 $fwdmer $2`
   count_rev=`agrep -c -s -V0 -2 $revmer $2`
   count_sum=$(($count_fwd + $count_rev))
   echo -e "$fwdmer\t$count_sum\t$count_fwd\t$count_rev" >> $1.counts
done
