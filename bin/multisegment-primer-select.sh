#!/bin/sh

# 15.05.2013 19:49:35 EDT
# Harm van Bakel <hvbakel@gmail.com>

####################################
# SELECT CANDIDATE 10-mer ADAPTERS #
####################################

# Location of non-redundant IRD
IRDREF=/projects/vanbah01a/reference-databases/influenza/InfluenzaReference.fa

# Generate a list of all possible 10-mers
generate-all-possible-nmers.pl 8 > ~/all-10mers.txt

# Count 10-mers in influenza reference database
jellyfish count -m 10 -s 5M $IRDREF
jellyfish dump mer_counts_0 | fasta2tabbed.pl - > ~/influenza-10mers.txt

# Collect 10-mers that are absent in any known influenza variant
ids-uniq2left -rc 2 ~/all-10mers.txt ~/influenza-10mers.txt  > ~/10mers-not-in-influenza.txt

# Exclude any candidates with homopolymer tracks
grep -vP 'AAA|GGG|CCC|TTT' ~/unique-10mers.txt > ~/10mers-not-in-influenza_filter1.txt

# Remove any potential matches with panhandle sequences
agrep -4 -v CAAAAGCAGG ~/10mers-not-in-influenza_filter1.txt > ~/10mers-not-in-influenza_filter2.txt
agrep -4 -v AGCAAAAGCA ~/10mers-not-in-influenza_filter2.txt > ~/10mers-not-in-influenza_filter3.txt
agrep -4 -v AGTAGAAACA ~/10mers-not-in-influenza_filter3.txt > ~/10mers-not-in-influenza_filter4.txt
agrep -4 -v AGAAACAAGG ~/10mers-not-in-influenza_filter4.txt > ~/10mers-not-in-influenza_filter5.txt

# Generate reference file for uniqueness check
fasta2tabbed.pl $IRDREF > ~/InfluenzaReference.tab

# Check 10-mer uniqueness in influenza when allowing for 2 mismatches
split -l 4000 ~/10mers-not-in-influenza_filter5.txt subset_ 
for i in subset_*
do
   influenza_ms-primer-uniqueness-check.sh $i ~/InfluenzaReference.tab &
done
cat *.counts | sort -k2,2n > ~/10mers-not-in-influenza_filter5.counts
rm -f subset_*

#######################
# CANDIDATE SELECTION #
#######################

# Potential candidates based on number of hits with 2 mismatches
GCGCGCGCGC
GGCGCGCGCG
ACGCGCCGCG
CGCCGTTACG
CGTTACGTCG
CGGTTAGCGC
TAAGCGCGCC
GTTACGCGCG
GTGCGGCTCG
GTTACGCGCC
ACGTCGTACG

# Selected set Opti1 (ePCR table for human/mouse check included below)
F1 GTTACGCGCC AGCAAAAGCAGG
F2 GTTACGCGCC AGCGAAAGCAGG
R1 GTTACGCGCC AGTAGAAACAAGG

hs-1	CGCCAGCAAAAGCAGG	GCCAGTAGAAACAAGG	100-2000
hs-2	CGCCAGCGAAAGCAGG	GCCAGTAGAAACAAGG	100-2000

# Selected set Opti2 (ePCR table for human/mouse check included below)
F1 GGCGCGCGCG AGCAAAAGCAGG
F2 GGCGCGCGCG AGCGAAAGCAGG
R1 GGCGCGCGCG AGTAGAAACAAGG

hs-3	CGCGAGCAAAAGCAGG	GCGAGTAGAAACAAGG	100-2000
hs-4	CGCGAGCGAAAGCAGG	GCGAGTAGAAACAAGG	100-2000

# Check uniqueness of shortened composite primers in the influenza genomes at various levels
agrep -c -s -V0 -2 GCGCCAGCAAAAGCAGG ~/InfluenzaReference.tab   # Opti1F, forward
agrep -c -s -V0 -2 CCTGCTTTTGCTGGCGC ~/InfluenzaReference.tab   # Opti1F, reverse-complement
agrep -c -s -V0 -2 GCGCCAGTAGAAACAAGG ~/InfluenzaReference.tab  # Opti1R, forward
agrep -c -s -V0 -2 CCTTGTTTCTACTGGCGC ~/InfluenzaReference.tab  # Opti1R, reverse-complement

agrep -c -s -V0 -2 GCGCGAGCAAAAGCAGG ~/InfluenzaReference.tab   # Opti2F, forward
agrep -c -s -V0 -2 CCTGCTTTTGCTCGCGC ~/InfluenzaReference.tab   # Opti2F, reverse-complement
agrep -c -s -V0 -2 GCGCGAGTAGAAACAAGG ~/InfluenzaReference.tab  # Opti2R, forward
agrep -c -s -V0 -2 CCTTGTTTCTACTCGCGC ~/InfluenzaReference.tab  # Opti2R, reverse-complement
