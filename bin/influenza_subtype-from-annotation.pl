#!/usr/bin/perl

# 29.07.2017 10:00:35 EDT
# Harm van Bakel <hvbakel@gmail.com>

# MODULES
use strict;
use warnings;
use Getopt::Long;

# GET PARAMETERS
my $sHelp          = 0;
my $sInputFile     = "";
my $sOutputPrefix  = "";
GetOptions("help!"    => \$sHelp,
           "input:s"  => \$sInputFile,
           "output:s" => \$sOutputPrefix);

# PRINT HELP
$sHelp = 1 unless($sInputFile and $sOutputPrefix);
if ($sHelp) {
   my $sScriptName = ($0 =~ /^.*\/(.+$)/) ? $1 : $0;
   die <<HELP

   Usage: $sScriptName
   
   Extract serotype information from annotation tool feature file.
    
   Arguments:
    -i -input <string>
      Features file from NIH flu annotation tool
    -o -output <string>
      Output file prefix
    -help
      This help message
   
HELP
}


##########
## MAIN ##
##########

my %hSerotype;
my $nSerotypeCount = 0;
open IN, $sInputFile or die "Error: can't open '$sInputFile': $!\n";
while (<IN>){
   next if (/^\s*$/);
   next if (/^ *#/);
   s/[\n\r]+$//;
   if (/ INFO: Serotype: ([HN])(\d+)$/){
      push @{$hSerotype{$1}}, $2;
      $nSerotypeCount++;
   }
}
close IN;

my $sSerotype = "";
if ($nSerotypeCount > 2){
   $sSerotype = "Mixed";
}
else{
   my $sH = exists($hSerotype{"H"}) ? "H" . $hSerotype{"H"}[0] : "Hx";
   my $sN = exists($hSerotype{"N"}) ? "N" . $hSerotype{"N"}[0] : "Nx";
   $sSerotype = "$sH$sN";
}

my $sOutputFile = $sOutputPrefix . "_subtype.txt";
open OUT, ">$sOutputFile" or die "Error: can't open '$sOutputFile': $!\n";
print OUT "$sOutputPrefix\t$sSerotype\n";
close OUT;
