#!/usr/bin/perl

# 24.10.2013 15:16:58 EDT
# Harm van Bakel <hvbakel@gmail.com>

# MODULES
use strict;
use warnings;
use Getopt::Long;
use IO::File;
use IO::Zlib qw(:gzip_external 1);
use File::Temp qw(tempfile tempdir);


# GLOBALS
$ENV{TMPDIR}         ||= "/sc/orga/scratch/cunhal01/tmp";        # location for tmp file storage
$SIG{'INT'}=$SIG{'HUP'}=$SIG{'ABRT'}=$SIG{'QUIT'}=$SIG{'TRAP'}=$SIG{'STOP'}=\&INTERRUPT;

# GET PARAMETERS
my $sHelp      = 0;
my $sInput     = "";
my $sBowtieRef = "";
my $nMaplength = 14;
my $nMinInsert = 250;
my $nThreads   = 12;
GetOptions("help!"        => \$sHelp,
           "fastq:s"      => \$sInput,
           "bowtie:s"     => \$sBowtieRef,
           "maplength:n"  => \$nMaplength,
           "insert:n"     => \$nMinInsert,
           "processors:n" => \$nThreads);

# PRINT HELP
$sHelp = 1 unless($sInput and $sBowtieRef);
if ($sHelp) {
   my $sScriptName = ($0 =~ /^.*\/(.+$)/) ? $1 : $0;
   die <<HELP

   Usage: $sScriptName
   
   Arguments:
    -f --fastq <string>
      Fastq input file to check for chimeric reads
    -b --bowtie <string>
      Bowtie indexed reference genome
    -m --maplength <integer>
      Length of segment to trim from each read for chimera mapping
      Default: $nMaplength
    -i --insert <integer>
      Minimum insert length for chimera detection. Read ends mapping
      to the reference genome with this distance will be flagged as
      chimeras.
      Default: $nMinInsert
    -p --processors <integer>
      Number of threads to use for bowtie mapping.
      Default: $nThreads
    -help
      This help message
   
HELP
}


##########
## MAIN ##
##########

# Open the input files
my ($fhInput);
if($sInput =~ /\.gz$/) {
   $fhInput = new IO::Zlib($sInput, "rb") or die "Error: can't open '$sInput': $!\n";
}
else{
   $fhInput = new IO::File($sInput, "r") or die "Error: can't open '$sInput': $!\n";
}

# Convert fastq file to tab-delimited file with end pairs
my $nReadCount = 0;
my ($fhTmp, $sTmp) = tempfile('fastq-ends-XXXXX', DIR=>$ENV{TMPDIR}, UNLINK=>1);
my $nSkipped = 0;
while(<$fhInput>) {
   my $sName   = $_;
   my $sSeq    = <$fhInput>;
   my $sTmp    = <$fhInput>;
   my $sQual   = <$fhInput>;
   $sName =~ s/[\n\r]+$//;
   $sSeq  =~ s/[\n\r]+$//;
   $sQual =~ s/[\n\r]+$//;
   print $fhTmp join("\t", $sName, substr($sSeq,0,$nMaplength), substr($sQual,0,$nMaplength), substr($sSeq,-$nMaplength), substr($sQual,-$nMaplength)), "\n";
   print $fhTmp join("\t", $sName, substr($sSeq,10,$nMaplength), substr($sQual,10,$nMaplength), substr($sSeq,-($nMaplength+10),$nMaplength), substr($sQual,-($nMaplength+10),$nMaplength)), "\n";
   $nReadCount++;
}
close $fhInput;
close $fhTmp;

# Map the tab-delimited fastq file and get any reads that map far apart
my %hMapped;
my %hChimeric;
open BOWTIE, "bowtie -n 3 --ff --seedlen $nMaplength --threads $nThreads -k 2 -M 1 --best --strata --maxins 3000 $sBowtieRef --12 $sTmp 2> /dev/null |" or die "Error: can't run bowtie: $!\n";
while (<BOWTIE>){
   next if (/^\s*$/);
   next if (/^ *#/);
   s/[\n\r]+$//;
   my ($sName1,$sStrand1,$sTarget1,$nPos1,$sSeq1,$sQual1,$sMM1,$sMMlist1) = split /\t/;
   $_ = <BOWTIE>;
   if ($_){
      my ($sName2,$sStrand2,$sTarget2,$nPos2,$sSeq2,$sQual2,$sMM2,$sMMlist2) = split /\t/;
      $sName1 = $sName1 =~ / / ? (split(/\s/,$sName1))[0] : substr($sName1, 0, -2);
      $sName2 = $sName2 =~ / / ? (split(/\s/,$sName2))[0] : substr($sName2, 0, -2);
      if ($sName1 eq $sName2){
         my $nFragmentSize = $nPos2>$nPos1 ? $nPos2 - $nPos1 + length($sSeq2) : $nPos1 - $nPos2 + length($sSeq1);
         if ($nFragmentSize > $nMinInsert){
            $hChimeric{$sName1} = $nFragmentSize;
         }
         $hMapped{$sName1}++;
      }
      else{
         die "Error: Paired read mismatch ($sName1, $sName2) on line $.\n";
      }
   }
   else{
      die "Error: Missing paired read at end of file\n";
   }
}
close BOWTIE;
foreach my $sKey (keys %hChimeric) {print "$sKey\t$hChimeric{$sKey}\n"}
my $nMappedReads  = keys(%hMapped);
my $nPctMapped    = sprintf("%.1f", 100*($nMappedReads/$nReadCount));
my $nChimericReads = keys(%hChimeric);
my $nPctChimeric   = sprintf("%.1f", 100*($nChimericReads/$nReadCount));
print STDERR "Mapped $nMappedReads out of $nReadCount reads ($nPctMapped%).\n";
print STDERR "Found $nChimericReads potential chimeric reads ($nPctChimeric%).\n";


#################
## SUBROUTINES ##
#################

# INTERRUPT
#
# Interrupt routine, make sure we exit gracefully for tmp file cleanup
sub INTERRUPT{
   exit(1); # This will call END
}
