#!/usr/bin/perl

# 14.05.2013 09:12:56 EDT
# Harm van Bakel <hvbakel@gmail.com>

# MODULES
use strict;
use warnings;
use Getopt::Long;
use File::Temp qw(tempfile tempdir);

# GLOBALS
$ENV{BLAT}   ||= "blat";
$ENV{TMPDIR} ||= "/tmp";
$SIG{'INT'}=$SIG{'HUP'}=$SIG{'ABRT'}=$SIG{'QUIT'}=$SIG{'TRAP'}=$SIG{'STOP'}=\&INTERRUPT;


# GET PARAMETERS
my $sHelp                = 0;
my $sInputFile           = "";
my $sBlatRef             = "/projects/vanbah01a/reference-databases/influenza/InfluenzaReference_cdhit.2bit";
my $nBlatScoreThresh     = 200;
my $nTransChimeraThresh  = 100;
my $nCisChimeraThresh    = 100;
my $nChimeraSeqBandwidth = 10;
my $sOutputPrefix        = "";
my $sVirusType           = "A";
my $flPrefilterOutput    = 0;
GetOptions("help!"       => \$sHelp,
           "fasta:s"     => \$sInputFile,
           "reference:s" => \$sBlatRef,
           "score:n"     => \$nBlatScoreThresh,
           "output:s"    => \$sOutputPrefix,
           "type:s"      => \$sVirusType,
           "prefilt!"    => \$flPrefilterOutput);

# PRINT HELP
$sHelp = 1 unless($sInputFile and $sBlatRef and $sOutputPrefix);
if ($sHelp) {
   my $sScriptName = ($0 =~ /^.*\/(.+$)/) ? $1 : $0;
   die <<HELP

   Usage: $sScriptName -f <contig-file> -r <blat-reference>
   
   Script to annotate influenza contigs using the influenza reference
   database. Detects and orients influenza genomic segments, trims
   sequences at panhandles, assesses completeness and chimeras.
   
   Arguments:
    -fasta <string>
      Fasta-formatted input file with contig sequences
    -reference <string>
      Non-redundant influenza reference database in 2bit format.
      default: $sBlatRef
    -score <integer>
      Score threshold for blat alignments.
      default: $nBlatScoreThresh
    -output <string>
      Output file prefix
    -type <string>
      Virus type (A or B). Default: $sVirusType
    -prefilt
      Prefilter output sequences to prune duplicate assemblies for same segment
    -help
      This help message
   
HELP
}


##########
## MAIN ##
##########

# Check arguments
die "Error: '$sInputFile' does not exist\n" unless ( -e $sInputFile);
die "Error: '$sBlatRef' does not exist\n" unless ( -e $sBlatRef);
die "Error: score threshold must be numeric\n" unless ($nBlatScoreThresh =~ /^\d+$/);

# Find and annotate influenza contigs
my $rhFluSegments = read_fasta_to_hash($sInputFile);
$rhFluSegments    = blat_orient_and_annotate_influenza_contigs($rhFluSegments, $sBlatRef, $nBlatScoreThresh);

# Trim at panhandle sequences and assess completeness
$rhFluSegments    = trim_sequences_at_panhandles($rhFluSegments, $sVirusType);

# Detect chimeric segments
$rhFluSegments    = blat_detect_chimeras($rhFluSegments, $sBlatRef, $nBlatScoreThresh, $nTransChimeraThresh, $nCisChimeraThresh, $nChimeraSeqBandwidth);

# Perform optional stringent output filtering to remove chimeric segments and segment fragments that are larger than a complete assembly of the same segment
if ($flPrefilterOutput){
   $rhFluSegments    = filter_segments($rhFluSegments);
}

# Write output
my %hSeqDuplicates;
open SEQ, ">$sOutputPrefix.fa" or die "Error: can't open '$sOutputPrefix.fa': $!\n";
open CHIM, ">$sOutputPrefix.chimerajunct" or die "Error: can't open '$sOutputPrefix.chimera': $!\n";
foreach my $sID (sort { $rhFluSegments->{$a}{segment} <=> $rhFluSegments->{$b}{segment} || length($rhFluSegments->{$b}{seq}) <=> length($rhFluSegments->{$a}{seq}) } keys %$rhFluSegments){
   my $sCompleteness  = $rhFluSegments->{$sID}{complete} ? "complete" : "fragment";
   my $sChimeraStatus = $rhFluSegments->{$sID}{chimera}  ? "chimera"  : "contiguous";
   my $sHeader = join("|", $rhFluSegments->{$sID}{segment}, $rhFluSegments->{$sID}{subtype}, $sCompleteness, $sChimeraStatus, $sID);
   my $sSeq    = $rhFluSegments->{$sID}{seq};
   $sSeq =~ s/.{100}/$&\n/sg;
   $sSeq =~ s/\n+$//;
   print SEQ ">$sHeader\n$sSeq\n" unless(exists $hSeqDuplicates{$sSeq});
   $hSeqDuplicates{$sSeq}++;
   foreach my $sChimeraSeq (@{$rhFluSegments->{$sID}{chimeraseq}}){
      print CHIM "$sChimeraSeq\n" if (length($sChimeraSeq)==(2*$nChimeraSeqBandwidth));
   }
}
close SEQ;
close CHIM;


#################
## SUBROUTINES ##
#################

# read_fasta_to_hash
#
# Read content of a fasta file into a hash with the sequenceID as
# the key and the sequence as the value.
sub read_fasta_to_hash {
   my ($sFASTA) = @_;
   
   my %hFasta;
   my $sFastaHeader = '';
   my $sFastaSeq    = '';
   open FASTA, "<$sFASTA" or die "Error: can't read the fasta file\n";
   while (<FASTA>){
      s/[\n\r]+$//;
      if (/^>/){
         die "Error: file ends in fasta header without sequence\n" if (eof);
         $sFastaSeq  =~ s/\s//g;
         die "Error: duplicate fasta ID '$sFastaHeader'\n" if (exists $hFasta{$sFastaHeader});
         $hFasta{$sFastaHeader}{seq} = $sFastaSeq if ($sFastaHeader);
         
         # Reset for the next sequence
         $sFastaHeader = $_;
         $sFastaHeader =~ s/^>\s*//;
         $sFastaHeader =~ s/\s.*$//;
         $sFastaSeq    = "";
      }
      elsif (eof){
         $sFastaSeq .= $_;
         $sFastaSeq  =~ s/\s//g;
         die "Error: duplicate fasta ID '$sFastaHeader'\n" if (exists $hFasta{$sFastaHeader});
         $hFasta{$sFastaHeader}{seq} = $sFastaSeq;
      }
      else{
         next if (/^\s*$/);
         next if (/^ *#/);
         $sFastaSeq .= $_ if ($sFastaHeader);
      }
   }
   close FASTA;
   return \%hFasta;
}


# write_fasta_hash_to_tmpfile
#
# Write a fasta hash to a temp file.
sub write_fasta_hash_to_tmpfile {
   my ($rhFasta) = @_;
   
   my ($fhTmp, $sTmp) = tempfile('fasta-XXXXX', DIR=>$ENV{TMPDIR}, UNLINK=>1);
   foreach my $sID (keys %$rhFasta){
      print $fhTmp ">$sID\n$rhFasta->{$sID}{seq}\n";
   }
   $fhTmp->close();
   return $sTmp;
}


# reverse_complement
#
# Returns the reverse-complement of the supplied sequence
sub reverse_complement{
   my $seq = shift(@_);
   my $rev = reverse $seq;
   $rev =~ tr/ACGTacgt/TGCAtgca/;
   return $rev;
}


# run_blat
#
# Runs blat and returns temporary file with the psl alignment results
sub run_blat {
   my ($sFasta, $sBlatRef, $nBlatScoreThresh) = @_;
   my ($fhTmp, $sTmp) = tempfile('blat-XXXXX', DIR=>$ENV{TMPDIR}, UNLINK=>1);
   $fhTmp->close();
   my $sBlatCommand = "blat -minScore=$nBlatScoreThresh -noHead -stepSize=5 -t=dna -q=dna $sBlatRef $sFasta $sTmp";
   system($sBlatCommand) == 0 or die "Error: blat command '$sBlatCommand' failed to run: $?\n";
   return $sTmp;
}


# blat_orient_and_annotate_influenza_contigs
#
# Takes a set of fasta sequences and annotates influenza contigs
# based on the best-hit match against a non-redundant influenza 
# reference database
sub blat_orient_and_annotate_influenza_contigs {
   my ($rhFasta, $sBlatRef, $nBlatScoreThresh) = @_;
   
   # Run blat
   my $sTmpFasta = write_fasta_hash_to_tmpfile($rhFasta);
   my $sTmpPsl   = run_blat($sTmpFasta, $sBlatRef, $nBlatScoreThresh);
   
   # Parse best hits from output
   my %hBestHits;
   open PSL, $sTmpPsl or die "Error: can't open blat output: $!\n";
   while (<PSL>){
      next if (/^\s*$/);
      next if (/^ *#/);
      s/[\n\r]+$//;
      my ($matches,$misMatches,$repMatches,$nCount,$qNumInsert,$qBaseInsert,$tNumInsert,$tBaseInsert,$strand,$qName,$qSize,$qStart,$qEnd,$tName,$tSize,$tStart,$tEnd,$blockCount,$blockSizes,$qStarts,$tStarts) = split /\t/;
      my $nScore = $matches+$repMatches-$misMatches-$qNumInsert-$tNumInsert;
      if (exists $hBestHits{$qName}){
         if($nScore > $hBestHits{$qName}{score}){
            $hBestHits{$qName}{score}  = $nScore;
            $hBestHits{$qName}{tName}  = $tName;
            $hBestHits{$qName}{strand} = $strand;
         }
      }
      else{
         $hBestHits{$qName}{score}  = $nScore;
         $hBestHits{$qName}{tName}  = $tName;
         $hBestHits{$qName}{strand} = $strand;
      }
   }
   close PSL;
   
   # Return properly oriented influenza contigs with segment annotation
   my %hReturn;
   foreach my $sID (keys %$rhFasta){
      if (exists $hBestHits{$sID}){
         my $sSeq     = $hBestHits{$sID}{strand} eq "+" ? $rhFasta->{$sID}{seq} : reverse_complement($rhFasta->{$sID}{seq});
         my ($nSegment, $tSysID, $tType, $tSubtype, $tOrganism) = split /\|/, $hBestHits{$sID}{tName};
         $hReturn{$sID}{seq}     = $sSeq;
         $hReturn{$sID}{segment} = $nSegment;
         $hReturn{$sID}{subtype} = $tSubtype;
      }
   }
   return \%hReturn;
}


# trim_sequences_at_panhandles
#
# Trim influenza segments at the panhandle sequences
sub trim_sequences_at_panhandles {
   my ($rhFasta, $sVirusType) = @_;

   # Define forward and reverse pan-handle sequences
   my @asPanFwd = ('GCAAAAGCAGG', 'GCGAAAGCAGG');
   my @asPanRev = ('CCTTGTTTCTACT','CTTTGTTTCTACT');
   
   if ($sVirusType eq "B"){
      @asPanFwd = ('GCAGAAGCGGA', 'GCAGAAGCGGT','GCAGAAGCAGA','GCAGAAGCACA','GCAGAAGCACG');
      @asPanRev = ('CGTGTTTCTACT', 'CTTGTTACTACT', 'GTTGTTTCTACT');
   }
               
               
   # Define full segment length range. This is based on the segment
   # length ranges observed in NCBI's flu resource and establishes
   # a cutoff for small fragments with pan handles at each end that
   # may be assembly errors
   my %hSegmentLengths;
   ($hSegmentLengths{1}{start}, $hSegmentLengths{1}{end}) = (2245, 2371);
   ($hSegmentLengths{2}{start}, $hSegmentLengths{2}{end}) = (2257, 2397);
   ($hSegmentLengths{3}{start}, $hSegmentLengths{3}{end}) = (2142, 2305);
   ($hSegmentLengths{4}{start}, $hSegmentLengths{4}{end}) = (807,  1886);
   ($hSegmentLengths{5}{start}, $hSegmentLengths{5}{end}) = (1464, 1845);
   ($hSegmentLengths{6}{start}, $hSegmentLengths{6}{end}) = (1005, 1560);
   ($hSegmentLengths{7}{start}, $hSegmentLengths{7}{end}) = (850,  1192);
   ($hSegmentLengths{8}{start}, $hSegmentLengths{8}{end}) = (802,  1098);
   
   # Process the sequences
   my %hReturn;
   foreach my $sID (keys %$rhFasta){
      my $sSeq     = $rhFasta->{$sID}{seq};
      my $nSegment = $rhFasta->{$sID}{segment};
      
      # Find forward matches
      my @anPanFwdMatches;
      foreach my $sPan (@asPanFwd){
         my @aaMatches = find_all_string_match_positions($sPan, $sSeq);
         foreach my $rMatch (@aaMatches){
            push @anPanFwdMatches, $rMatch->[0];
         }
      }
      
      # Trim forward
      if (@anPanFwdMatches){
         @anPanFwdMatches = sort {$a <=> $b} @anPanFwdMatches;
         if (@anPanFwdMatches > 1){
            # If there is a second match within 100nt, trim down to that match instead
            if ( ($anPanFwdMatches[1] - $anPanFwdMatches[0]) <= 100 ){
               $sSeq = substr $sSeq, $anPanFwdMatches[1];
            }
            else{
               $sSeq = substr $sSeq, $anPanFwdMatches[0];
            }
         }
         else{
            $sSeq = substr $sSeq, $anPanFwdMatches[0];
         }
         $sSeq = "A$sSeq";
      }
      
      # Find reverse matches
      my @anPanRevMatches;
      foreach my $sPan (@asPanRev){
         my @aaMatches = find_all_string_match_positions($sPan, $sSeq);
         foreach my $rMatch (@aaMatches){
            push @anPanRevMatches, $rMatch->[1];
         }
      }
      
      # Trim reverse
      if (@anPanRevMatches){
         @anPanRevMatches = sort {$b <=> $a} @anPanRevMatches;
         if (@anPanRevMatches > 1){
            # If there is a second match within 100nt of the first, trim down to that match instead
            if ( ($anPanRevMatches[0] - $anPanRevMatches[1]) <= 100 ){
               $sSeq = substr $sSeq, 0, $anPanRevMatches[1];
            }
            else{
               $sSeq = substr $sSeq, 0, $anPanRevMatches[0];
            }
         }
         else{
            $sSeq = substr $sSeq, 0, $anPanRevMatches[0];
         }
      }
      
      # Return trimmed fragments
      $hReturn{$sID}{seq}     = $sSeq;
      $hReturn{$sID}{segment} = $nSegment;
      $hReturn{$sID}{subtype} = $rhFasta->{$sID}{subtype};
      my $nSeqLength = length($sSeq);
      if (@anPanFwdMatches and @anPanRevMatches){
         if (exists $hSegmentLengths{$nSegment}){
            if ( ($nSeqLength >= $hSegmentLengths{$nSegment}{start}) and ($nSeqLength <= $hSegmentLengths{$nSegment}{end}) ){
               $hReturn{$sID}{complete} = 1;
            }
            else{
               $hReturn{$sID}{complete} = 0;
            }
         }
         else{
            die "Error: missing segment info for $sID\n";
         }
      }
      else{
         $hReturn{$sID}{complete} = 0;
      }
   }
   return \%hReturn;
}

# filter_segments
#
# Prefilter segments to remove chimeric segments and fragments longer than complete assemblies of the same segment
sub filter_segments {
   my ($rhFasta) = @_;
   
   # For each segment, find the largest complete assembly and record its ID and length
   my %hCompleteSegments;
   foreach my $sID (keys %$rhFasta){
      my $sSeq       = $rhFasta->{$sID}{seq};
      my $nSegment   = $rhFasta->{$sID}{segment};
      my $flComplete = $rhFasta->{$sID}{complete};
      
      if ($flComplete){
         if (exists $hCompleteSegments{$nSegment}){
            if ( length($sSeq) > $hCompleteSegments{$nSegment}{length} ){
               $hCompleteSegments{$nSegment}{id}     = $sID;
               $hCompleteSegments{$nSegment}{length} = length($sSeq);
            }
         }
         else{
            $hCompleteSegments{$nSegment}{id}     = $sID;
            $hCompleteSegments{$nSegment}{length} = length($sSeq);
         }
      }
   }
   
   # Filter output
   my %hReturn;
   foreach my $sID (keys %$rhFasta){
      my $sSeq       = $rhFasta->{$sID}{seq};
      my $nSegment   = $rhFasta->{$sID}{segment};
      my $flComplete = $rhFasta->{$sID}{complete};
      my $flChimera  = $rhFasta->{$sID}{chimera};
      
      if ( exists $hCompleteSegments{$nSegment} ){
         if( length($sSeq) <=  $hCompleteSegments{$nSegment}{length}){
            $hReturn{$sID} = $rhFasta->{$sID} unless $flChimera;
         }
      }
      else{
         $hReturn{$sID} = $rhFasta->{$sID} unless $flChimera;
      }
   }
   
   return \%hReturn;
}



# find_all_string_match_positions
#
# Returns all match positions of a regexp in a target string
sub find_all_string_match_positions {
   my ($regex, $string) = @_;
   my @ret;
   while ($string =~ /($regex)/g) {
      my $nEnd   = pos($string);
      my $nStart = $nEnd - length($1);
      push @ret, [$nStart, $nEnd];
   }
   return @ret
}


# blat_detect_chimeras
#
# Detect chimeric contigs by matching against a non-redundant
# influenza reference database
sub blat_detect_chimeras {
   my ($rhFasta, $sBlatRef, $nBlatScoreThresh, $nTransChimeraThresh, $nCisChimeraThresh, $nChimeraSeqBandwidth) = @_;

   # Run blat
   my $sTmpFasta = write_fasta_hash_to_tmpfile($rhFasta);
   my $sTmpPsl   = run_blat($sTmpFasta, $sBlatRef, $nBlatScoreThresh);

   # Collect best-hit matches
   my %hBestHits;
   open PSL, $sTmpPsl or die "Error: can't open blat output: $!\n";
   while (<PSL>){
      next if (/^\s*$/);
      next if (/^ *#/);
      s/[\n\r]+$//;
      my ($matches,$misMatches,$repMatches,$nCount,$qNumInsert,$qBaseInsert,$tNumInsert,$tBaseInsert,$strand,$qName,$qSize,$qStart,$qEnd,$tName,$tSize,$tStart,$tEnd,$blockCount,$blockSizes,$qStarts,$tStarts) = split /\t/;
      my $nScore = $matches+$repMatches-$misMatches-$qNumInsert-$tNumInsert;
      if (exists $hBestHits{$qName}){
         if($nScore > $hBestHits{$qName}{score}){
            my %tmp;
            @tmp{qw(score qSize qStart qEnd blockCount blockSizes qStarts tStarts qBaseInsert tBaseInsert)} = ($nScore,$qSize,$qStart,$qEnd,$blockCount,$blockSizes,$qStarts,$tStarts,$qBaseInsert,$tBaseInsert);
            $hBestHits{$qName} = {%tmp};
         }
      }
      else{
         my %tmp;
         @tmp{qw(score qSize qStart qEnd blockCount blockSizes qStarts tStarts qBaseInsert tBaseInsert)} = ($nScore,$qSize,$qStart,$qEnd,$blockCount,$blockSizes,$qStarts,$tStarts,$qBaseInsert,$tBaseInsert);
         $hBestHits{$qName} = {%tmp};
      }
   }
   close PSL;
   
   # Look for chimeras and identify where the chimera junctions are
   my %hReturn = %$rhFasta;
   foreach my $sID (keys %hBestHits){
      my %hHit = %{$hBestHits{$sID}};
      my $nQspan = $hHit{qEnd} - $hHit{qStart};
      my @anChimeraJunctions;
      
      # Chimera option one: partial query match to best-hit segment
      if ( ($hHit{qSize} - $nQspan) > $nTransChimeraThresh ){
         my $nChimeraJunction = $hHit{qStart} < ($hHit{qSize} - $hHit{qEnd}) ? $hHit{qEnd} : $hHit{qStart};
         push @anChimeraJunctions, $nChimeraJunction;
      }
      
      # Chimera option two: full query match, but with a large deletion in the query
      if($hHit{tBaseInsert} > $nCisChimeraThresh){
         if ($hHit{blockCount} > 1){
            my @anBlockSizes = split /,/, $hHit{blockSizes};
            my @anQstarts    = split /,/, $hHit{qStarts};
            my @anTstarts    = split /,/, $hHit{tStarts};
            
            foreach (my $i=0 ; $i<($hHit{blockCount}-1) ; $i++){
               my $nGap = $anTstarts[$i+1] - ($anTstarts[$i] + $anBlockSizes[$i]);
               if ($nGap > $nCisChimeraThresh){
                  push @anChimeraJunctions, $anQstarts[$i] + $anBlockSizes[$i];
               }
            }
         }
      }
      
      # Annotate chimeric junction sequences in result hash
      if (@anChimeraJunctions){
         $hReturn{$sID}{chimera}    = 1;
         $hReturn{$sID}{chimerapos} = [@anChimeraJunctions];
         foreach my $nJunction (@anChimeraJunctions){
            my $nJunctionSeqStart = $nJunction < $nChimeraSeqBandwidth ? $nJunction : $nJunction - $nChimeraSeqBandwidth;
            my $sJunctionSeq = substr $hReturn{$sID}{seq}, $nJunctionSeqStart, 2*$nChimeraSeqBandwidth;
            push @{$hReturn{$sID}{chimeraseq}}, $sJunctionSeq;
         }
      }
      else{
         $hReturn{$sID}{chimera}    = 0;
         $hReturn{$sID}{chimerapos} = [];
         $hReturn{$sID}{chimeraseq} = [];
      }
   }
   return \%hReturn;
}


# INTERRUPT
#
# Interrupt routine, make sure we exit gracefully for tmp file cleanup
sub INTERRUPT{
   exit(1); # This will call END
}
