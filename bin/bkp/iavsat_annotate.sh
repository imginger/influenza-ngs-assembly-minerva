#!/bin/sh

retries=10;
while [ $retries -gt 0 ]
do
   curl --proxy proxy.mgmt.hpc.mssm.edu:8123 --data-urlencode sequence@${1}.fa http://www.ncbi.nlm.nih.gov/genomes/FLU/Database/annotation.cgi > ${1}_temp.txt
   
   cat ${1}_temp.txt | awk "/id='tbl'/ {p=1}; p; /id='xml'/ {p=0}" > ${1}_feature_temp.txt
   
   #parse the first line, starting with &gt;Feature and add the rest of the line to the temporary file
   #add the rest of the matching lines, from line 2 to the end and then remove the last line (which contains the xml div after the table we're interestedin)
   #finally replace the url encoded "&gt;" with ">" and save to the final .anno file
   a=$(head -1 ${1}_feature_temp.txt)
   b=$(echo $a | awk -F"<pre>&gt;" '{print ">"$2}')
   echo $b > ${1}.anno
   tail -n +2 ${1}_feature_temp.txt | head -n -1 >> ${1}.anno 
   sed -i 's/&gt;/>/g' ${1}.anno
   
   cat ${1}_temp.txt | awk "/ffn_h/ {p=1}; p; /aln_h/ {p=0}" > ${1}_cds_temp.txt
   a=$(head -1 ${1}_cds_temp.txt)
   b=$(echo $a | awk -F"&gt;" '{print ">"$2}')
   echo $b > ${1}_cds.fa
   tail -n +2 ${1}_cds_temp.txt | head -n -1 >> ${1}_cds.fa
   sed -i 's/&gt;/>/g' ${1}_cds.fa
   
   rm -f ${1}_*temp.txt

   # Given the frequent issues retrieving data from the annotation tool, see if we get any output, otherwise try again
   annofilesize=$(stat -c%s "${1}.anno")
   if [ $annofilesize -gt 5 ]
   then
      retries=0
   else
      retries=$[$retries-1]
      sleep 5
   fi
done
