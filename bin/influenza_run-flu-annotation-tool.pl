#!/usr/bin/perl

# 25.07.2017 13:22:46 EDT
# Harm van Bakel <hvbakel@gmail.com>

# MODULES
use strict;
use warnings;
use Getopt::Long;

# ENVIRONMENT
$ENV{'CURL_CA_BUNDLE'} = "/etc/ssl/certs/ca-bundle.crt";

# GET PARAMETERS
my $sHelp         = 0;
my $sInput        = "";
my $sOutputPrefix = "";
my $sURL          = "https://www.ncbi.nlm.nih.gov/genomes/FLU/annotation/api/";
my $sProxy        = "proxy.mgmt.hpc.mssm.edu:8123";
my $nRetries      = 5;
GetOptions("help!"     => \$sHelp,
           "input:s"   => \$sInput,
           "output:s"  => \$sOutputPrefix,
           "url:s"     => \$sURL,
           "proxy:s"   => \$sProxy,
           "retries=i" => \$nRetries);

# PRINT HELP
$sHelp = 1 unless($sInput);
if ($sHelp) {
   my $sScriptName = ($0 =~ /^.*\/(.+$)/) ? $1 : $0;
   die <<HELP

   Usage: $sScriptName
   
   Use the NCBI flu annotation tool to annotate influenza sequences.
   
   Arguments:
    -i -input <string>
      Name of fasta file to annotate
    -o -output <string>
      Output prefix
    -u -url <string>
      Url for the flu annotation tool. Default: $sURL
    -p -proxy <string>
      Web proxy url. Default: $sProxy
    -r -retries <integer>
      Number of times to attempt data retrieval. Default: $nRetries
    -help
      This help message
   
HELP
}


##########
## MAIN ##
##########

# Check inputs
die "Error: number of retries must be an integer greater than zero\n" unless ( $nRetries =~ /^\d+$/ and $nRetries > 0);

# Set default output prefix
unless($sOutputPrefix){
   $sOutputPrefix = $sInput;
   $sOutputPrefix =~ s/(.fasta|.fa)$//i;
}

# Submit sequence and get job ID
my $sJobID = "";
my $sCurlSubmit = join("", "curl -X POST -F 'sequence=@", $sInput, "' -F 'cmd=submit' ", $sURL);
$sCurlSubmit   .= " --proxy $sProxy" if ($sProxy);
my $nSubmissionRetries = $nRetries;
while($nSubmissionRetries){
   open SUBMIT, "$sCurlSubmit |" or die "Error: could not open curl: $!\n";
   while (<SUBMIT>){
      if (/(JSID.*_annotation)/){
         $sJobID = $1;
      }
   }
   close SUBMIT;
   
   # Retry if submission failed
   if ($sJobID){
      $nSubmissionRetries = 0;
   }
   else{
      $nSubmissionRetries--;
      sleep 5
   }
}

# Download and parse results
if ($sJobID){
   warn "Job submitted successfully with job ID: $sJobID\n";
   my $sResultTable     = curl_download($sJobID, 'tbl', $sProxy, $nRetries);
   my $sResultCDS       = curl_download($sJobID, 'ffn', $sProxy, $nRetries);
   
   if ($sResultTable and $sResultCDS){
      my $rProteinMappings = get_protein_id_mapping($sResultTable);
      my $rCDS             = fasta_to_hash($sResultCDS);
      
      # Print annotation table
      open OUTTABLE, ">${sOutputPrefix}.features_table.txt" or die "Error: can't open '${sOutputPrefix}.features_table.txt': $!\n";
      print OUTTABLE $sResultTable;
      close OUTTABLE;
      
      # Print CDS sequences
      open OUTCDS, ">${sOutputPrefix}.features_cds.fa" or die "Error: can't open '${sOutputPrefix}.features_cds.fa': $!\n";
      foreach my $sID (sort keys %$rCDS){
         if (exists $rProteinMappings->{$sID} ){
            my $sHeader    = ">" . $rProteinMappings->{$sID} . "|" . $sID;
            my $sSequence  = "";
            my $nSeqLength = length($rCDS->{$sID}); 
            for (my $i = 0; $i < $nSeqLength; $i += 100){
               $sSequence .= substr($rCDS->{$sID}, $i, 100) . "\n";
            }
            print OUTCDS "$sHeader\n$sSequence";
         }
         else{
            die "Error: can't match protein ID '$sID' to a product name\n";
         }
      }
      close OUTCDS;
      
      # Print protein sequences
      open OUTPROT, ">${sOutputPrefix}.features_protein.fa" or die "Error: can't open '${sOutputPrefix}.features_protein.fa': $!\n";
      foreach my $sID (sort keys %$rCDS){
         if (exists $rProteinMappings->{$sID} ){
            my $sHeader   = ">" . $rProteinMappings->{$sID} . "|" . $sID;
            my $sSequence = translate($rCDS->{$sID});
            print OUTPROT "$sHeader\n$sSequence";
         }
         else{
            die "Error: can't match protein ID '$sID' to a product name\n";
         }
      }
      close OUTPROT;
   }
   else{
      die "Error: could not retrieve feature table and/or cds sequences, even after trying $nRetries times.\n";
   }
}
else{
   die "Error: flu annotation tool job submission failed. Did not receive a job ID.\n";
}

#################
## SUBROUTINES ##
#################


# curl_download
#
# Download table and sequence data for a submitted job ID
sub curl_download {
   my ($sJobID, $sFormat, $sProxy, $nRetries) = @_;

   my $sCurlResults = "curl -X POST -F 'cmd=download' -F 'job_id=$sJobID' -F 'format=$sFormat' $sURL";
   $sCurlResults   .= " --proxy $sProxy" if ($sProxy);
   
   # Download the requested result
   my $sResult = "";
   while($nRetries){
      sleep 5;
      open RESULTS, "$sCurlResults  |" or die "Error: could not open curl: $!\n";
      while (<RESULTS>){
         $sResult .= $_;
      }
      close RESULTS;
      
      # Check if the job has completed
      if ($sResult =~ /Job is not complete. Status is/){
         $nRetries--;
         $sResult = "";
      }
      else{
         $nRetries = 0;
      }
   }
   return $sResult;
}

# get_protein_id_mapping
#
# Get mapping of protein ID to product name
sub get_protein_id_mapping {
   my ($sTable) = @_;
   
   my %hReturn;
   my ($sGene, $sID) = ("", "");
   my @asLines = split /\n/, $sTable;
   foreach my $sLine (@asLines){
      if ($sLine =~ /CDS\t\t$/){
         ($sGene, $sID) = ("", "");
      }
      if ($sLine =~ /\t\t\tprotein_id\t(.*)$/){
         $sID = $1;
      }
      if ($sLine =~ /\t\t\tgene\t(\S+)$/){
         $sGene = $1;
      }     
      if ($sGene and $sID){
         die "Error: found duplicate protein ID in table file\n" if (exists $hReturn{$sID});
         $hReturn{$sID} = $sGene;
         ($sGene, $sID) = ("", "");
      }
   }
   return \%hReturn;
}


# fasta_to_hash
#
# Parse the content of a fasta string into a hash with the sequenceID as
# the key and the sequence as the value.
sub fasta_to_hash {
   my ($sFasta) = @_;
   
   my %hFasta;
   my $sFastaHeader = '';
   my $sFastaSeq    = '';
   my @asFasta = split /\n/, $sFasta;
   for ( my $i = 0 ; $i < @asFasta ; $i++){
      if ($asFasta[$i] =~ /^>/){
         die "Error: file ends in fasta header without sequence\n" if ($i == $#asFasta);
         $sFastaSeq  =~ s/\s//g;
         die "Error: duplicate fasta ID '$sFastaHeader'\n" if (exists $hFasta{$sFastaHeader});
         $hFasta{$sFastaHeader} = $sFastaSeq if ($sFastaHeader);
         
         # Reset for the next sequence
         $sFastaHeader = $asFasta[$i];
         $sFastaHeader =~ s/^>\s*//;
         $sFastaHeader =~ s/\s.*$//;
         $sFastaSeq    = "";
      }
      elsif ($i == $#asFasta){
         $sFastaSeq .= $asFasta[$i];
         $sFastaSeq  =~ s/\s//g;
         die "Error: duplicate fasta ID '$sFastaHeader'\n" if (exists $hFasta{$sFastaHeader});
         $hFasta{$sFastaHeader} = $sFastaSeq;
      }
      else{
         next if ($asFasta[$i] =~ /^\s*$/);
         next if ($asFasta[$i] =~ /^ *#/);
         $sFastaSeq .= $asFasta[$i] if ($sFastaHeader);
      }
   }
   return \%hFasta;
}

# translate
#
# Translate coding sequence
sub translate {
  my ($sSeq) = (@_);

   my %codon =  
   (
   "TTT" => "F", "TTC" => "F", "TTA" => "L", "TTG" => "L",
   "TCT" => "S", "TCC" => "S", "TCA" => "S", "TCG" => "S",
   "TAT" => "Y", "TAC" => "Y", "TAA" => "*", "TAG" => "*",
   "TGT" => "C", "TGC" => "C", "TGA" => "*", "TGG" => "W",
   "CTT" => "L", "CTC" => "L", "CTA" => "L", "CTG" => "L",
   "CCT" => "P", "CCC" => "P", "CCA" => "P", "CCG" => "P",
   "CAT" => "H", "CAC" => "H", "CAA" => "Q", "CAG" => "Q",
   "CGT" => "R", "CGC" => "R", "CGA" => "R", "CGG" => "R",
   "ATT" => "I", "ATC" => "I", "ATA" => "I", "ATG" => "M",
   "ACT" => "T", "ACC" => "T", "ACA" => "T", "ACG" => "T",
   "AAT" => "N", "AAC" => "N", "AAA" => "K", "AAG" => "K",
   "AGT" => "S", "AGC" => "S", "AGA" => "R", "AGG" => "R",
   "GTT" => "V", "GTC" => "V", "GTA" => "V", "GTG" => "V",
   "GCT" => "A", "GCC" => "A", "GCA" => "A", "GCG" => "A",
   "GAT" => "D", "GAC" => "D", "GAA" => "E", "GAG" => "E",
   "GGT" => "G", "GGC" => "G", "GGA" => "G", "GGG" => "G",
   );

   # Translate sequence
   my $nStart = 0;
   my $nEnd = length($sSeq);
   my $aaseq;
   for (my $i = 0; $i+2 < $nEnd; $i+=3) {
      my $aa = $codon{uc(substr($sSeq, $i, 3))};
      $aaseq .= $aa;
   }

   # Pretty-print sequence
   my $aaReturn = "";
   my $aalen = length($aaseq); 
   for (my $i = 0; $i < $aalen; $i += 100){
      $aaReturn .= substr($aaseq, $i, 100) . "\n";
   }
   return $aaReturn;
}
