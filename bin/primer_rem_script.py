#!/usr/bin/env python

# -*- coding: utf-8 -*-
"""
@Created: 07-13-2016 
@Description: This script removes the forward and reverse primers from the curated varpatch fasta files

"""
from collections import defaultdict
import sys


def usage():
	return "usage:"+"\n"+"\tprimer_rem_script.py <curated varpatch fasta file> <output file>"

if len(sys.argv)!=3:
	print usage()
	sys.exit(0)

#sort the list according to length of the strings: largest to smallest
def lengthsort(a):
    n = len(a)
    for i in range(n):
        for j in range(i+1,n):
            if len(a[i]) < len(a[j]):
                temp = a[i]
                a[i] = a[j]
                a[j] = temp
    return a


def remove_primers(fastafile):
   
    f = open(fastafile, "r")
    
    mydict=defaultdict(str)
    name = ''
    
    #forward primer1
    string1 = 'GTTACGCGCCAGCAAAAGCAGG'
    #forward primer2
    string2 = 'GTTACGCGCCAGCGAAAGCAGG'
    #reverse primer
    string3 = 'CCTTGTTTCTACTGGCGC' 

    length1 = len(string1)
    length2 = len(string2)
    length3 = len(string3)
    len_cutoff1 = length1 - 11
    len_cutoff2 = length2 - 11
    len_cutoff3 = length3 - 11

    #get list of forward primers 1&2
    alist = []
    alist3 = []

    for i in xrange(len_cutoff1):
        for j in xrange(i,len_cutoff1):
          alist.append(string1[i:j + 12]) 

    for i in xrange(len_cutoff2):
        for j in xrange(i,len_cutoff2):
          alist.append(string2[i:j + 12])

    for i in xrange(len_cutoff3):
        for j in xrange(i,len_cutoff3):
          alist3.append(string3[i:j + 13])


    alist = lengthsort(alist)
    alist3 = lengthsort(alist3)
    list_length = len(alist)
    list_length3 = len(alist3)

    #put headers and sequences in a dictionary
    for line in f:
        #if line starts with > then it is the header
        if line.startswith('>'):
            name = line[1:-1]
            continue #skip to the next line
        #executed if it is a sequence and not a header
        mydict[name]+=line.strip()

    mydict_len = len(mydict)
    
    #remove forward primers
    for x in range(0,(mydict_len)):
        for s in range(0,(list_length-1)):
            #if alist[s] in my_file_contents:
            if mydict.values()[x].startswith(alist[s]):
                len1 = len(alist[s])
                mydict[mydict.keys()[x]] = mydict.values()[x][len1:]

    #removing reverse primer
    for x in range(0,(mydict_len)):
        for s in range(0,(list_length3-1)):
            if mydict.values()[x].endswith(alist3[s]):
                len3 = len(alist3[s])
                mydict[mydict.keys()[x]] = mydict.values()[x][:-len3]
    
    f.close()
    return mydict

#write the trimmed sequences to a new file
def createFile(fastafile, outfile):
    file=open(outfile,"a")
    mydict1 = remove_primers(fastafile)
    all_headers = mydict1.keys()
    all_headers = sorted(all_headers)

    for i in range(0, len(mydict1)):
        header = all_headers[i]
        file.write(">")
        file.write(header)
        file.write("\n")
        sequence = mydict1.get(all_headers[i])
        file.write(sequence)
        file.write("\n")

    file.close()
    return file

 
if __name__ == "__main__":

    fastafile = sys.argv[1]
    outfile = sys.argv[2]

    try:
    	createFile(fastafile, outfile)
    except:
	print "Error reading fastafile\n" 
	print usage()
	sys.exit(0)
