#!/usr/bin/perl

# 22.08.2013 19:00:44 EDT
# Harm van Bakel <hvbakel@gmail.com>

# MODULES
use strict;
use warnings;
use Getopt::Long;

# GET PARAMETERS
my $sHelp    = 0;
my $sBam     = "";
my $sFasta   = "";
my $nDepth   = 150000;
GetOptions("help!"   => \$sHelp,
           "bam:s"   => \$sBam,
           "fasta:s" => \$sFasta,
           "depth:n" => \$nDepth);

# PRINT HELP
$sHelp = 1 unless($sBam or $sFasta);
if ($sHelp) {
   my $sScriptName = ($0 =~ /^.*\/(.+$)/) ? $1 : $0;
   die <<HELP

   Usage: $sScriptName -b <bam-file> -f <fasta-file>
   
   Make sure the consensus sequence consists of the most frequent base
   in the sequencing data.
   
   Arguments:
    -b --bam <string>
     Bam file
    -f --fasta <string>
     Fasta file
    -d --depth <integer>
     Max read depth to consider
    -help
      This help message
   
HELP
}


##########
## MAIN ##
##########

open IN, "samtools mpileup -d $nDepth $sBam -f $sFasta |" or die "Error: can't run mpileup: $!\n";
print "#id\tpos\tAltRatio\tRefBase\tReadSum\tRefFwd\tRefRev\tAfwd\tArev\tCfwd\tCrev\tGfwd\tGrev\tTfwd\tTrev\n";
while (<IN>){
   next if (/^\s*$/);
   next if (/^ *#/);
   s/[\n\r]+$//;
   my ($sID, $nPos, $sRefBase, $nReadCount, $sAltString, $sQstring) = split /\t/;
   my $nRefFwd = $sAltString =~ tr/././;
   my $nRefRev = $sAltString =~ tr/,/,/;
   my $nAfwd   = $sAltString =~ tr/A/A/;
   my $nArev   = $sAltString =~ tr/a/a/;
   my $nCfwd   = $sAltString =~ tr/C/C/;
   my $nCrev   = $sAltString =~ tr/c/c/;
   my $nGfwd   = $sAltString =~ tr/G/G/;
   my $nGrev   = $sAltString =~ tr/g/g/;
   my $nTfwd   = $sAltString =~ tr/T/T/;
   my $nTrev   = $sAltString =~ tr/t/t/;
   my $nReadSum = $nRefFwd +$nRefRev + $nAfwd + $nArev + $nCfwd + $nCrev + $nGfwd + $nGrev + $nTfwd + $nTrev;
   my $nAltRatio = $nReadSum ? ($nReadSum - $nRefFwd - $nRefRev)/$nReadSum : 0;
   print join("\t", $sID, $nPos, $nAltRatio, $sRefBase, $nReadSum, $nRefFwd, $nRefRev, $nAfwd, $nArev, $nCfwd, $nCrev, $nGfwd, $nGrev, $nTfwd, $nTrev), "\n";
}
close IN;
