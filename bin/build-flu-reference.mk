#!/usr/bin/make -Rf

# Harm van Bakel <harm.vanbakel@mssm.edu>
# Divya Kriti <divya.kriti@mssm.edu>

###############
# DEFINE ARGS #
###############

type        ?=A
awkt        :=awk -F '\t' -v OFS='\t'
sortt       :=sort -t $$'\t'

##############
# CHECK ARGS #
##############

ifeq ($(type),A)
	STARindex=12
else
	STARindex=9
endif


ifndef in
$(error missing argument in)
else
   name?=$(in:.fa=)
endif

#########
# RULES #
#########

all: $(name)_filt_cdhit_assembly-check.fa \
     $(name)_filt_cdhit_chimera-check.fa \
     $(name)_filt_cdhit_assembly-check.2bit \
     $(name)_filt_cdhit_chimera-check_bowtie \
     $(name)_filt_cdhit_chimera-check_STAR

###################
# BUILD REFERENCE #
###################

# Filter low-quality sequences
%_filt.fa: %.fa
	filter-IRD-sequences.pl $< > $@ 2> $@.log

# Collapse sequences if they are >= 97% identical at the sequence level (for segment check blat index)
%_filt_cdhit_assembly-check.fa: %_filt.fa
	cd-hit-est -i $< -o $@ -G 0 -c 0.97 -M 0 -T 2 -s 0 -aS 0.97 -r 1

# Collapse sequences if they are >= 99% identical at the sequence level (for chimera check bowtie index)
%_filt_cdhit_chimera-check.fa: %_filt.fa
	cd-hit-est -i $< -o $@ -G 0 -c 0.99 -M 0 -T 2 -s 0 -aS 0.97 -r 1

# Build blat index
%_filt_cdhit_assembly-check.2bit: %_filt_cdhit_assembly-check.fa
	faToTwoBit $< $@

# Build bowtie index
%_filt_cdhit_chimera-check_bowtie: %_filt_cdhit_chimera-check.fa
	mkdir $@
	bowtie-build $< $@/$(patsubst %.fa,%,$<)

# Build STAR index
%_filt_cdhit_chimera-check_STAR: %_filt_cdhit_chimera-check.fa
	mkdir $@
	STAR --runMode genomeGenerate --runThreadN 8 --genomeDir $@ --genomeFastaFiles $< --genomeSAindexNbases $(STARindex) --genomeChrBinNbits 11
	mv Log.out $@
